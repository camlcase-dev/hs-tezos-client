{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeApplications    #-}
{-# LANGUAGE TypeInType          #-}

module Main where

import Aeson.Hspec (aesonEqualitySpec, checkGoldenFileSpec)
import Arbitrary ()
import Data.Proxy (Proxy(Proxy))
import Test.Hspec (hspec, parallel)

import Tezos.Client.Delphi.Block
import Tezos.Client.Delphi.BigMap
import Tezos.Client.Delphi.Vote
import Tezos.Client.Delphi.Micheline.Expression
import Tezos.Client.Mutez

main :: IO ()
main = do
  hspec $ do
    parallel $ do      
      -- Tezos.Client.Types.Micheline
      checkGoldenFileSpec (Proxy @Expression) "golden/getContractStorage.json"

      -- Tezos.Client.Types.BlockHeader
      checkGoldenFileSpec (Proxy @Block) "golden/getBlock.json"
      checkGoldenFileSpec (Proxy @Block) "golden/getBlock2.json"
      checkGoldenFileSpec (Proxy @Block) "golden/getBlock3.json"
      checkGoldenFileSpec (Proxy @Block) "golden/getBlock4.json"
      checkGoldenFileSpec (Proxy @Block) "golden/getBlock5.json"
      checkGoldenFileSpec (Proxy @Block) "golden/getBlock6.json"
      checkGoldenFileSpec (Proxy @Block) "golden/getBlock7.json"
      checkGoldenFileSpec (Proxy @Block) "golden/getBlock8.json"
      -- block 1168849
      checkGoldenFileSpec (Proxy @Block) "golden/getBlock9.json"      
      -- block 1288177
      checkGoldenFileSpec (Proxy @Block) "golden/getBlock10.json"
      
      checkGoldenFileSpec (Proxy @Block) "golden/transactionBlock.json"
      checkGoldenFileSpec (Proxy @Block) "golden/transactionBlock2.json"
      checkGoldenFileSpec (Proxy @Block) "golden/transactionBlock7.json"


      -- -- Tezos.Client.Types.Micheline
      checkGoldenFileSpec (Proxy @Parameters) "golden/parameters0.json"

      -- -- Tezos.Client.Types.Core
      aesonEqualitySpec $ Proxy @Ballot
      aesonEqualitySpec $ Proxy @BigMapDiff
      aesonEqualitySpec $ Proxy @Entrypoint
      aesonEqualitySpec $ Proxy @Error
      aesonEqualitySpec $ Proxy @Mutez
      aesonEqualitySpec $ Proxy @Expression

      aesonEqualitySpec $ Proxy @Block
      aesonEqualitySpec $ Proxy @BalanceUpdate
      aesonEqualitySpec $ Proxy @BlockHeaderLevel
      aesonEqualitySpec $ Proxy @BlockHeaderMetadata
      aesonEqualitySpec $ Proxy @Delegation
      aesonEqualitySpec $ Proxy @FullHeader
      aesonEqualitySpec $ Proxy @InternalOperationResult
      aesonEqualitySpec $ Proxy @(InternalOperationResultsMetadata Int)
      aesonEqualitySpec $ Proxy @MaxOperationLength
      aesonEqualitySpec $ Proxy @OperationSeedNonceRevelation
      aesonEqualitySpec $ Proxy @OperationDoubleEndorsementEvidence
      aesonEqualitySpec $ Proxy @OperationDoubleBakingEvidence      
      aesonEqualitySpec $ Proxy @OperationActivateAccount
      aesonEqualitySpec $ Proxy @OperationProposals
      aesonEqualitySpec $ Proxy @OperationBallot
      aesonEqualitySpec $ Proxy @OperationReveal
      aesonEqualitySpec $ Proxy @OperationTransaction
      aesonEqualitySpec $ Proxy @OperationOrigination
      aesonEqualitySpec $ Proxy @OperationDelegation
      aesonEqualitySpec $ Proxy @Operation
      aesonEqualitySpec $ Proxy @OperationContents
      aesonEqualitySpec $ Proxy @OperationContentsAndResult
      aesonEqualitySpec $ Proxy @OperationEndorsementMetadata
      aesonEqualitySpec $ Proxy @Origination
      aesonEqualitySpec $ Proxy @Parameters
      aesonEqualitySpec $ Proxy @RawBlockHeader
      aesonEqualitySpec $ Proxy @Reveal
      aesonEqualitySpec $ Proxy @TestChainStatus
      aesonEqualitySpec $ Proxy @Transaction

      aesonEqualitySpec $ Proxy @Vote
      aesonEqualitySpec $ Proxy @BallotCount
      aesonEqualitySpec $ Proxy @VotingPeriodKind
      aesonEqualitySpec $ Proxy @DelegateWeight
