{-# LANGUAGE AllowAmbiguousTypes       #-}
{-# LANGUAGE DataKinds                 #-}
{-# LANGUAGE DeriveGeneric             #-}
{-# LANGUAGE DuplicateRecordFields     #-}
{-# LANGUAGE FlexibleContexts          #-}
{-# LANGUAGE NoMonomorphismRestriction #-}
{-# LANGUAGE TypeApplications          #-}

module Lens where

-- import Control.Lens as Lens
-- import Data.Generics.Product (the, getField, field)
-- import Data.Generics.Sum (_Ctor)
-- import Tezos.Client.Delphi.Block
-- import GHC.Generics (Generic)

-- abc :: Block -> [Operation]
-- abc = Lens.view traverse . (the @[[Operation]])

-- abc :: Block -> [[Operation]]
-- abc = getField @"operations"

-- ddd :: [[Operation]] -> [Operation]
-- ddd x = x & Lens.traverse . Lens.traverse . field @"operations"

-- eee :: Block -> [[Operation]]
-- eee = Lens.view (field @"operations")

-- foo :: Operation -> Maybe OperationAndResult
-- foo x = x Lens.^? (_Ctor @"OperationAndResult")


-- data X = X {x :: Int} deriving (Eq,Show,Generic)
-- data Y = Y {y :: String} deriving (Eq,Show,Generic)
-- data FooBar = Foo X | Bar Y deriving (Eq,Show,Generic)

-- bar :: [[FooBar]] -> [X]
-- bar x = x Lens.& traverse . traverse . _Ctor @"Foo" Lens.^. field @"x"
