{-# OPTIONS_GHC -Wno-orphans #-}

module Arbitrary where

import Data.Aeson (Value(Null))
import Data.Char (isAlpha)

import GHC.Natural

import Test.QuickCheck (Arbitrary(arbitrary), Gen, choose, elements, oneof, suchThat, vector, vectorOf)
import Test.QuickCheck.Instances.ByteString ()
import Test.QuickCheck.Instances.Text ()
import Test.QuickCheck.Instances.Time ()

import Tezos.Client.Delphi.BigMap
import Tezos.Client.Delphi.Block
import Tezos.Client.Delphi.Endorsement
import Tezos.Client.Delphi.Vote
import Tezos.Client.Delphi.Micheline.Primitive
import Tezos.Client.Delphi.Micheline.Expression
import Tezos.Client.Mutez
import Tezos.Client.Timestamp

instance Arbitrary Natural where
  arbitrary =
    fromInteger <$> choose (0 , 999999999999999999)

instance Arbitrary Timestamp where
  arbitrary = Timestamp <$> arbitrary

instance Arbitrary Vote where
  arbitrary = Vote <$> arbitrary <*> arbitrary

instance Arbitrary BallotCount where
  arbitrary = BallotCount <$> arbitrary <*> arbitrary <*> arbitrary

instance Arbitrary VotingPeriodKind where
  arbitrary =
    elements
      [ Proposal
      , TestingVote
      , Testing
      , PromotionVote
      ]

instance Arbitrary DelegateWeight where
  arbitrary = DelegateWeight <$> arbitrary <*> arbitrary


instance Arbitrary Endorsement where
  arbitrary = Endorsement <$> arbitrary <*> arbitrary <*> arbitrary

instance Arbitrary EndorsementContents where
  arbitrary = EndorsementContents <$> arbitrary

instance Arbitrary BigMapDiffUpdate where
  arbitrary = BMDU <$> arbitrary <*> arbitrary <*> arbitrary <*> arbitrary

instance Arbitrary BigMapDiffRemove where
  arbitrary = BMDR <$> arbitrary

instance Arbitrary BigMapDiffCopy where
  arbitrary = BMDC <$> arbitrary <*> arbitrary

instance Arbitrary BigMapDiffAlloc where
  arbitrary = BMDA <$> arbitrary <*> arbitrary <*> arbitrary

instance Arbitrary BigMapDiff where
  arbitrary =
    oneof
      [ BigMapDiffUpdate <$> arbitrary
      , BigMapDiffRemove <$> arbitrary
      , BigMapDiffCopy   <$> arbitrary
      , BigMapDiffAlloc  <$> arbitrary
      ]

instance Arbitrary Ballot where
  arbitrary = elements [Yay, Nay, Pass]

-- arbitrary :: Gen [a]

arbitraryAlphaChar :: Gen Char
arbitraryAlphaChar = suchThat arbitrary isAlpha


arbitraryAlphaString :: Gen String
arbitraryAlphaString = do
  k <- choose (1,25)
  vectorOf k arbitraryAlphaChar

instance Arbitrary Entrypoint where
  arbitrary =
    oneof
      [ pure EDefault
      , pure ERoot
      , pure EDo
      , pure ESetDelegate
      , pure ERemoveDelegate
      , EString <$> arbitrary
      ]

instance Arbitrary Error where
  arbitrary = pure $ Error Null

instance Arbitrary Contract where
  arbitrary = Contract <$> arbitrary <*> arbitrary

instance Arbitrary MichelinePrim where
  arbitrary = do
    j <- choose (0,2)
    k <- choose (0,2)
    MichelinePrim <$> arbitrary
                  <*> oneof [pure Nothing, Just <$> vector j]
                  <*> oneof [pure Nothing, Just <$> vector k]

instance Arbitrary Expression where
  arbitrary = do
    j <- choose (0,2)
    oneof
      [ ExpressionInt    <$> arbitrary
      , ExpressionString <$> arbitrary
      , ExpressionBytes  <$> arbitrary
      , Expressions      <$> vector j
      , ExpressionPrim   <$> arbitrary
      ]

instance Arbitrary Primitive where
  arbitrary =
    oneof
      [ PrimitiveInstruction <$> arbitrary
      , PrimitiveData <$> arbitrary
      , PrimitiveType <$> arbitrary
      ]

instance Arbitrary PrimitiveInstruction where
  arbitrary =
    elements
      [ PIAdd
      , PILe
      , PIUnit
      , PICompare
      , PILambda
      , PILoop
      , PIImplicitAccount
      , PINone
      , PIBlake2B
      , PISha256
      , PIXor
      , PIRename
      , PIMap
      , PISetDelegate
      , PIDip
      , PIPack
      , PISize
      , PIIfCons
      , PILsr
      , PITransferTokens
      , PIUpdate
      , PICdr
      , PISwap
      , PISome
      , PISha512
      , PICheckSignature
      , PIBalance
      , PIEmptySet
      , PISub
      , PIMem
      , PIRight
      , PIAddress
      , PIConcat
      , PIUnpack
      , PINot
      , PILeft
      , PIAmount
      , PIDrop
      , PIAbs
      , PIGe
      , PIPush
      , PILt
      , PINeq
      , PINeg
      , PICons
      , PIExec
      , PINil
      , PIIsnat
      , PIMul
      , PILoopLeft
      , PIEdiv
      , PISlice
      , PIStepsToQuota
      , PIInt
      , PISource
      , PICar
      , PICreateAccount
      , PILsl
      , PIOr
      , PIIfNone
      , PISelf
      , PIIf
      , PISender
      , PIDup
      , PIEq
      , PINow
      , PIGet
      , PIGt
      , PIIfLeft
      , PIFailwith
      , PIPair
      , PIIter
      , PICast
      , PIEmptyMap
      , PICreateContract
      , PIHashKey
      , PIContract
      , PIAnd
      ]

instance Arbitrary PrimitiveData where
  arbitrary =
    elements
      [ PDElt
      , PDRight
      , PDFalse
      , PDUnit
      , PDSome
      , PDNone
      , PDLeft
      , PDTrue
      , PDPair
      ]

instance Arbitrary PrimitiveType where
  arbitrary =
    elements
      [ PTTimestamp
      , PTSignature
      , PTSet
      , PTPair
      , PTBytes
      , PTAddress
      , PTOr
      , PTList
      , PTStorage
      , PTKeyHash
      , PTUnit
      , PTOption
      , PTBigMap
      , PTString
      , PTMutez
      , PTBool
      , PTOperation
      , PTContract
      , PTMap
      , PTNat
      , PTKey
      , PTLambda
      , PTInt
      , PTParameter
      , PTCode
      ]

-- instance Arbitrary InternalByteString where
--   arbitrary = InternalByteString <$> arbitrary

instance Arbitrary Mutez where
  arbitrary =
    unsafeMkMutez <$> choose (0, 9223372036854775807)

instance Arbitrary Block where
  arbitrary = do
    k <- choose (0,2)
    Block <$> arbitrary <*> arbitrary <*> arbitrary <*> arbitrary <*>
      arbitrary <*> ((:[]) <$> vector k)

instance Arbitrary RawBlockHeader where
  arbitrary = RawBlockHeader <$> arbitrary <*> arbitrary <*> arbitrary <*>
    arbitrary <*> arbitrary <*> arbitrary <*> arbitrary <*> arbitrary <*>
    arbitrary <*> arbitrary <*> arbitrary

instance Arbitrary FullHeader where
  arbitrary = FullHeader <$> arbitrary <*> arbitrary <*> arbitrary <*>
    arbitrary <*> arbitrary <*> arbitrary <*> arbitrary <*> arbitrary <*>
    arbitrary <*> arbitrary <*> arbitrary <*> arbitrary

instance Arbitrary BlockHeaderMetadata where
  arbitrary = do
    j <- choose (0,2)
    k <- choose (0,2)
    l <- choose (0,2)
    BlockHeaderMetadata <$> arbitrary <*> arbitrary <*> arbitrary <*> arbitrary
      <*> arbitrary <*> arbitrary <*> vector j <*> arbitrary <*> arbitrary
      <*> arbitrary <*> arbitrary <*> arbitrary <*> vector k <*> vector l

instance Arbitrary TestChainStatus where
  arbitrary =
    oneof
      [ pure TCSNotRunning
      , TCSForking <$> arbitrary <*> arbitrary
      , TCSRunning <$> arbitrary <*> arbitrary <*> arbitrary <*> arbitrary
      ]

instance Arbitrary MaxOperationLength where
  arbitrary = MaxOperationLength <$> arbitrary <*> arbitrary

instance Arbitrary BlockHeaderLevel where
  arbitrary = BlockHeaderLevel <$> arbitrary <*> arbitrary <*> arbitrary
    <*> arbitrary <*> arbitrary <*> arbitrary <*> arbitrary

instance Arbitrary BalanceUpdate where
  arbitrary =
    oneof
      [ BalanceUpdateContract <$> arbitrary <*> arbitrary
      , FreezerRewards <$> arbitrary <*> arbitrary <*> arbitrary
      , FreezerFees <$> arbitrary <*> arbitrary <*> arbitrary
      , FreezerDeposits <$> arbitrary <*> arbitrary <*> arbitrary
      ]

instance Arbitrary OperationAndResult where
  arbitrary = do
    k <- choose (0,2)
    OAR <$> arbitrary <*> arbitrary <*> arbitrary
      <*> arbitrary <*> vector k <*> arbitrary

instance Arbitrary OperationWithoutResult where
  arbitrary = do
    k <- choose (0,2)
    OWR <$> arbitrary <*> arbitrary <*> arbitrary
      <*> arbitrary <*> vector k <*> arbitrary

instance Arbitrary Operation where
  arbitrary =
    oneof
      [ OperationAndResult <$> arbitrary
      , Operation <$> arbitrary
      ]

instance Arbitrary OperationSeedNonceRevelation where
  arbitrary = OperationSeedNonceRevelation <$> arbitrary <*> arbitrary

instance Arbitrary OperationDoubleEndorsementEvidence where
  arbitrary = OperationDoubleEndorsementEvidence <$> arbitrary <*> arbitrary

instance Arbitrary OperationDoubleBakingEvidence where
  arbitrary = OperationDoubleBakingEvidence <$> arbitrary <*> arbitrary

instance Arbitrary OperationActivateAccount where
  arbitrary = OperationActivateAccount <$> arbitrary <*> arbitrary

instance Arbitrary OperationProposals where
  arbitrary = do
    k <- choose (0,2)
    OperationProposals <$> arbitrary <*> arbitrary <*> vector k

instance Arbitrary OperationBallot where
  arbitrary =
    OperationBallot <$> arbitrary <*> arbitrary <*> arbitrary <*> arbitrary

instance Arbitrary OperationReveal where
  arbitrary =
    OperationReveal <$> arbitrary <*> arbitrary <*> arbitrary <*> arbitrary
    <*> arbitrary <*> arbitrary

-- instance Arbitrary Parameters where
--   arbitrary = Parameters <$> arbitrary <*> arbitrary

instance Arbitrary OperationTransaction where
  arbitrary =
    OperationTransaction <$> arbitrary <*> arbitrary <*> arbitrary <*> arbitrary
    <*> arbitrary <*> arbitrary <*> arbitrary <*> arbitrary

instance Arbitrary OperationOrigination where
  arbitrary =
    OperationOrigination <$> arbitrary <*> arbitrary <*> arbitrary <*> arbitrary
    <*> arbitrary <*> arbitrary <*> arbitrary  <*> arbitrary

instance Arbitrary OperationDelegation where
  arbitrary =
    OperationDelegation <$> arbitrary <*> arbitrary <*> arbitrary <*> arbitrary
    <*> arbitrary <*> arbitrary

instance Arbitrary OperationContents where
  arbitrary =
    oneof
      [ OperationContentsEndorsement <$> arbitrary
      , OperationContentsSeedNonceRevelation <$> arbitrary
      , OperationContentsDoubleEndorsementEvidence <$> arbitrary
      , OperationContentsDoubleBakingEvidence <$> arbitrary
      , OperationContentsActivateAccount <$> arbitrary
      , OperationContentsProposals <$> arbitrary
      , OperationContentsBallot <$> arbitrary
      , OperationContentsReveal <$> arbitrary
      , OperationContentsTransaction <$> arbitrary 
      , OperationContentsOrigination <$> arbitrary
      , OperationContentsDelegation  <$> arbitrary
      ]

instance Arbitrary OperationContentsAndResult where
  arbitrary = do
    oneof
      [ OperationContentsAndResultEndorsement <$> arbitrary <*> arbitrary
      , OperationContentsAndResultSeedNonceRevelation <$> arbitrary <*> arbitrary
      , OperationContentsAndResultDoubleEndorsementEvidence <$> arbitrary <*> arbitrary
      , OperationContentsAndResultDoubleBakingEvidence <$> arbitrary <*> arbitrary
      , OperationContentsAndResultActivateAccount <$> arbitrary <*> arbitrary
      , OperationContentsAndResultProposals <$> arbitrary
      , OperationContentsAndResultBallot <$> arbitrary
      , OperationContentsAndResultReveal <$> arbitrary <*> arbitrary
      , OperationContentsAndResultTransaction <$> arbitrary <*> arbitrary
      , OperationContentsAndResultOrigination <$> arbitrary <*> arbitrary 
      , OperationContentsAndResultDelegation  <$> arbitrary <*> arbitrary
      ]

instance Arbitrary BalanceUpdates where
  arbitrary = do 
    j <- choose (0,2)
    BalanceUpdates <$> vector j

instance Arbitrary OperationEndorsementMetadata where
  arbitrary = do
    j <- choose (0,2)
    k <- choose (0,2)
    OperationEndorsementMetadata <$> vector j <*> arbitrary <*> vector k

instance (Arbitrary a) => Arbitrary (InternalOperationResultsMetadata a) where
  arbitrary = do
    j <- choose (0,2)
    k <- choose (0,2)
    results <- vector k
    InternalOperationResultsMetadata <$> vector j <*> arbitrary <*> elements [Nothing, Just results]

instance Arbitrary InternalOperationResult where
  arbitrary = do
    oneof
      [ InternalOperationResultReveal <$> arbitrary <*> arbitrary <*> arbitrary <*> arbitrary
      , InternalOperationResultTransaction <$> arbitrary <*> arbitrary <*> arbitrary <*> arbitrary <*> arbitrary <*> arbitrary
      , InternalOperationResultOrigination <$> arbitrary <*> arbitrary <*> arbitrary <*> arbitrary <*> arbitrary <*> arbitrary
      , InternalOperationResultDelegation <$> arbitrary <*> arbitrary <*> arbitrary <*> arbitrary
      ]

instance Arbitrary Delegation where
  arbitrary = do
    k      <- choose (0,2)
    errors' <- vector k
    oneof
      [ DelegationApplied <$> arbitrary <*> arbitrary
      , pure $ DelegationFailed errors'
      , pure DelegationSkipped
      , DelegationBacktracked <$> arbitrary <*> arbitrary <*> elements [Nothing, Just errors'] 
      ]

instance Arbitrary Reveal where
  arbitrary = do
    k      <- choose (0,2)
    errors' <- vector k
    oneof
      [ RevealApplied <$> arbitrary <*> arbitrary
      , pure $ RevealFailed errors'
      , pure RevealSkipped
      , RevealBacktracked <$> arbitrary <*> arbitrary <*> elements [Nothing, Just errors']
      ]

instance Arbitrary Origination where
  arbitrary = do
    j              <- choose (0,2)
    bigMapDiffs' <- vector j
    k              <- choose (0,2)
    balanceUpdates' <- vector k
    l              <- choose (0,2)
    contractIds    <- vector l
    i              <- choose (0,2)
    errors'         <- vector i
    oneof
      [ OriginationApplied <$> elements [Nothing, Just bigMapDiffs'] <*> elements [Nothing, Just balanceUpdates'] <*> elements [Nothing, Just contractIds] <*> arbitrary <*> arbitrary <*> arbitrary <*> arbitrary
      , pure $ OriginationFailed errors'
      , pure OriginationSkipped
      , OriginationBacktracked <$> elements [Nothing, Just errors'] <*> elements [Nothing, Just bigMapDiffs'] <*> elements [Nothing, Just balanceUpdates'] <*> elements [Nothing, Just contractIds]  <*> arbitrary <*> arbitrary <*> arbitrary <*> arbitrary
      ]

instance Arbitrary Transaction where
  arbitrary = do
    k              <- choose (0,2)
    balanceUpdates' <- vector k
    l              <- choose (0,2)
    contractIds    <- vector l
    i              <- choose (0,2)
    errors'         <- vector i
    oneof
      [ TransactionApplied <$> arbitrary <*> arbitrary <*> elements [Nothing, Just balanceUpdates'] <*> elements [Nothing, Just contractIds] <*> arbitrary <*> arbitrary <*> arbitrary <*> arbitrary <*> arbitrary
      , pure $ TransactionFailed errors'
      , pure TransactionSkipped
      , TransactionBacktracked <$> elements [Nothing, Just errors'] <*> arbitrary <*> arbitrary <*> elements [Nothing, Just balanceUpdates'] <*> elements [Nothing, Just contractIds]  <*> arbitrary <*> arbitrary <*> arbitrary <*> arbitrary <*> arbitrary
      ]


instance Arbitrary Parameters where
  arbitrary = Parameters <$> arbitrary <*> arbitrary
