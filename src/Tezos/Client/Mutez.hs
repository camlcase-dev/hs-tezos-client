{-# LANGUAGE DeriveDataTypeable         #-}
{-# LANGUAGE DeriveGeneric              #-}
{-# LANGUAGE DerivingStrategies         #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE LambdaCase                 #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE ScopedTypeVariables        #-}
{-# LANGUAGE ViewPatterns               #-}

module Tezos.Client.Mutez
  (   -- * Mutez
    Mutez (unMutez)
  , mkMutez
  , mkMutez'
  , unsafeMkMutez
  , toMutez
  , addMutez
  , unsafeAddMutez
  , subMutez
  , unsafeSubMutez
  , mulMutez
  , divModMutez
  , divModMutezInt
  , mutezToInt64
  , zeroMutez
  , oneMutez
  , prettyTez
  ) where

import qualified Data.Aeson as Aeson
import Data.Bifunctor (bimap, first)
import Data.Data (Data(..))
import Data.Int (Int64)
import Data.Maybe (fromMaybe)
import Data.Scientific (FPFormat( Fixed ), floatingOrInteger, formatScientific)
import qualified Data.Text as T
import Data.Word (Word32, Word64)
import Fmt (pretty)
import GHC.Generics (Generic)
import GHC.Stack (HasCallStack)
import Safe (readMay)
                        
--------------------------------------------------------------------------------
-- Mutez
--------------------------------------------------------------------------------

-- | Mutez is a wrapper over integer data type. 1 mutez is 1 token (μTz).
newtype Mutez = Mutez
  { unMutez :: Word64
  } deriving stock (Show, Eq, Ord, Data, Generic)
    deriving newtype (Enum)

instance Aeson.ToJSON Mutez where
  toJSON = Aeson.toJSON . show . unMutez

instance Aeson.FromJSON Mutez where
  parseJSON = Aeson.withText "Mutez" $ \s ->
    case readMay (T.unpack s) :: Maybe Word64 of
      Just i  -> pure $ Mutez i
      Nothing -> error "Unable to parse Mutez. Expected a string encoded Word64."

instance Bounded Mutez where
  minBound = Mutez 0
  -- 2⁶³ - 1
  maxBound = Mutez 9223372036854775807

-- | Safely create 'Mutez' checking for overflow.
mkMutez :: Word64 -> Maybe Mutez
mkMutez n
  | n <= unMutez maxBound = Just (Mutez n)
  | otherwise = Nothing
{-# INLINE mkMutez #-}

-- | Partial function for 'Mutez' creation, it's pre-condition is that
-- the argument must not exceed the maximal 'Mutez' value.
unsafeMkMutez :: HasCallStack => Word64 -> Mutez
unsafeMkMutez n =
  fromMaybe (error $ "mkMutez: overflow (" <> show n <> ")") (mkMutez n)
{-# INLINE unsafeMkMutez #-}

-- | Safely create 'Mutez'.
toMutez :: Word32 -> Mutez
toMutez = unsafeMkMutez . fromIntegral
{-# INLINE toMutez #-}

-- | Convert between integral types, checking for overflows/underflows.
fromIntegralChecked :: (Integral a, Integral b) => a -> Either T.Text b
fromIntegralChecked _a = undefined

-- | Version of 'mkMutez' that accepts a number of any type.
mkMutez' :: Integral i => i -> Either T.Text Mutez
mkMutez' i = do
  w :: Word64 <- fromIntegralChecked i
  maybe (Left "Mutez overflow") Right (mkMutez w)

-- | Addition of 'Mutez' values. Returns 'Nothing' in case of overflow.
addMutez :: Mutez -> Mutez -> Maybe Mutez
addMutez (unMutez -> a) (unMutez -> b) =
  mkMutez (a + b) -- (a + b) can't overflow if 'Mutez' values are valid
{-# INLINE addMutez #-}

-- | Partial addition of 'Mutez', should be used only if you're
-- sure there'll be no overflow.
unsafeAddMutez :: HasCallStack => Mutez -> Mutez -> Mutez
unsafeAddMutez x = fromMaybe (error "unsafeAddMutez: overflow") . addMutez x

-- | Subtraction of 'Mutez' values. Returns 'Nothing' when the
-- subtrahend is greater than the minuend, and 'Just' otherwise.
subMutez :: Mutez -> Mutez -> Maybe Mutez
subMutez (unMutez -> a) (unMutez -> b)
  | a >= b = Just (Mutez (a - b))
  | otherwise = Nothing
{-# INLINE subMutez #-}

-- | Partial subtraction of 'Mutez', should be used only if you're
-- sure there'll be no underflow.
unsafeSubMutez :: HasCallStack => Mutez -> Mutez -> Mutez
unsafeSubMutez x = fromMaybe (error "unsafeSubMutez: underflow") . subMutez x

-- | Multiplication of 'Mutez' and an integral number. Returns
-- 'Nothing' in case of overflow.
mulMutez :: Integral a => Mutez -> a -> Maybe Mutez
mulMutez (unMutez -> a) b
    | res <= toInteger (unMutez maxBound) = Just (Mutez (fromInteger res))
    | otherwise = Nothing
  where
    res = toInteger a * toInteger b
{-# INLINE mulMutez #-}

-- | Euclidian division of two 'Mutez' values.
divModMutez :: Mutez -> Mutez -> Maybe (Word64, Mutez)
divModMutez a (unMutez -> b) = first unMutez <$> divModMutezInt a b

-- | Euclidian division of  'Mutez' and a number.
divModMutezInt :: Integral a => Mutez -> a -> Maybe (Mutez, Mutez)
divModMutezInt (toInteger . unMutez -> a) (toInteger -> b)
  | b <= 0 = Nothing
  | otherwise = Just $ bimap toMutez' toMutez' (a `divMod` b)
  where
    toMutez' :: Integer -> Mutez
    toMutez' = Mutez . fromInteger

-- | Convert mutez to signed number.
mutezToInt64 :: Mutez -> Int64
mutezToInt64 = fromIntegral . unMutez

-- | 0 XTZ
zeroMutez :: Mutez
zeroMutez = Mutez minBound

-- | 0.000001 XTZ
oneMutez :: Mutez
oneMutez = Mutez 1

-- |
-- >>> prettyTez (toMutez 420)
-- "0.00042 ꜩ"
-- >>> prettyTez (toMutez 42000000)
-- "42 ꜩ"
prettyTez :: Mutez -> T.Text
prettyTez ((/ 1000000) . fromIntegral . unMutez -> s) =
  case floatingOrInteger s of
    Left (_ :: Float)    -> T.pack $ formatScientific Fixed Nothing s
    Right (n :: Integer) -> pretty n
  <> " ꜩ"
