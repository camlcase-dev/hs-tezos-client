{-# LANGUAGE DeriveGeneric     #-}
{-# LANGUAGE OverloadedStrings #-}

module Tezos.Client.Delphi.Endorsement where

import Data.Aeson
import Data.Text (Text)
import GHC.Generics (Generic)

data Endorsement =
  Endorsement
    { branch     :: Text
    , operations :: EndorsementContents
    , signature  :: Maybe Text
    } deriving (Eq, Show, Generic)

instance ToJSON Endorsement where
  toJSON (Endorsement branch' operations' signature') =
    object
      [ "branch"     .= branch'
      , "operations" .= operations'
      , "signature"  .= signature'
      ]

instance FromJSON Endorsement where
  parseJSON = withObject "Endorsement" $ \o ->
    Endorsement <$> o .: "branch"
                <*> o .: "operations"
                <*> o .: "signature"

newtype EndorsementContents =
  EndorsementContents
    { level :: Int
    } deriving (Eq, Show, Generic)

instance ToJSON EndorsementContents where
  toJSON (EndorsementContents level') =
    object
      [ "kind"  .= ("endorsement" :: Text)
      , "level" .= level'
      ]

instance FromJSON EndorsementContents where
  parseJSON = withObject "EndorsementContents" $ \o ->
    EndorsementContents <$> o .: "level"
