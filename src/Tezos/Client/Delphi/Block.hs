{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE OverloadedStrings     #-}

module Tezos.Client.Delphi.Block where

import Data.Aeson
  (FromJSON(parseJSON), ToJSON(toJSON), Value(Array, Object), (.=),
   (.:), (.:?), object, withObject)
import Data.Int (Int64)
import Data.Maybe (catMaybes)
import Data.Text (Text)
import qualified Data.Vector as V
import Data.Word (Word8, Word16)
import GHC.Generics (Generic)
import GHC.Natural (Natural)
import qualified Data.HashMap.Lazy as HM
import Tezos.Client.Delphi.Micheline.Expression (Expression, Parameters, Contract)
import Tezos.Client.Delphi.BigMap
import Tezos.Client.Delphi.Endorsement
import Tezos.Client.Delphi.Vote
import Tezos.Client.Mutez
import Tezos.Client.Timestamp
import Tezos.Client.Util.Aeson (StringEncode(..), (.=?), combineValues)

newtype Error =
  Error
    { unError :: Value
    } deriving (Eq, Show, Generic)

instance ToJSON Error where
  toJSON (Error error') = error'

instance FromJSON Error where
  parseJSON = pure . Error

data Block =
  Block
    { protocol   :: Text
    , chainId    :: Text
    , hash       :: Text
    , header     :: RawBlockHeader
    , metadata   :: BlockHeaderMetadata
    , operations :: [[Operation]]
    } deriving (Eq, Show, Generic)

instance ToJSON Block where
  toJSON (Block protocol' chainId' hash' header' metadata' operations') =
    object
      [ "protocol"   .= protocol'
      , "chain_id"   .= chainId'
      , "hash"       .= hash'
      , "header"     .= header'
      , "metadata"   .= metadata'
      , "operations" .= operations'
      ]

instance FromJSON Block where
  parseJSON = withObject "Block" $ \o ->
    Block <$> o .: "protocol"
          <*> o .: "chain_id"
          <*> o .: "hash"
          <*> o .: "header"
          <*> o .: "metadata"
          <*> o .: "operations"

data FullHeader =
  FullHeader
    { level            :: Int64
    , proto            :: Word8
    , predecessor      :: Text
    , timestamp        :: Timestamp
    , validationPass   :: Word8
    , operationsHash   :: Text
    , fitness          :: [Text]
    , context          :: Text
    , priority         :: Word16
    , proofOfWorkNonce :: Text
    , seedNonceHash    :: Maybe Text
    , signature        :: Text
    } deriving (Eq, Show, Generic)

instance ToJSON FullHeader where
  toJSON (FullHeader level' proto' predecessor' timestamp' validationPass'
          operationsHash' fitness' context' priority' proofOfWorkNonce'
          seedNonceHash' signature') =
    object $
      [ "level"               .= level'
      , "proto"               .= proto'
      , "predecessor"         .= predecessor'
      , "timestamp"           .= timestamp'
      , "validation_pass"     .= validationPass'
      , "operations_hash"     .= operationsHash'
      , "fitness"             .= fitness'
      , "context"             .= context'
      , "priority"            .= priority'
      , "proof_of_work_nonce" .= proofOfWorkNonce'
      , "signature"           .= signature'
      ] ++ catMaybes ["seed_nonce_hash" .=? seedNonceHash']

instance FromJSON FullHeader where
  parseJSON = withObject "FullHeader" $ \o ->
    FullHeader <$> o .:  "level"
               <*> o .:  "proto"
               <*> o .:  "predecessor"
               <*> o .:  "timestamp"
               <*> o .:  "validation_pass"
               <*> o .:  "operations_hash"
               <*> o .:  "fitness"
               <*> o .:  "context"
               <*> o .:  "priority"
               <*> o .:  "proof_of_work_nonce"
               <*> o .:? "seed_nonce_hash"
               <*> o .:  "signature"

data BlockHeaderMetadata =
  BlockHeaderMetadata
    { protocol                 :: Text
    , nextProtocol             :: Text
    , testChainStatus          :: TestChainStatus
    , maxOperationsTtl         :: Int -- not string encoded
    , maxOperationDataLength   :: Int
    , maxBlockHeaderLength     :: Int
    , maxOperationListLength   :: [MaxOperationLength]
    , baker                    :: Text
    , level                    :: BlockHeaderLevel
    , votingPeriodKind         :: VotingPeriodKind
    , nonceHash                :: Maybe Text
    , consumedGas              :: Natural -- PositiveBignum
    , deactivated              :: [Text]
    , balanceUpdates           :: [BalanceUpdate]
    } deriving (Eq, Show, Generic)

instance ToJSON BlockHeaderMetadata where
  toJSON (BlockHeaderMetadata protocol' nextProtocol' testChainStatus'
          maxOperationsTtl' maxOperationsDataLength' maxBlockHeaderLength'
          maxOperationListLength' baker' level' votingPeriodKind' nonceHash'
          consumedGas' deactivated' balanceUpdates') =
    object
      [ "protocol"                   .= protocol'
      , "next_protocol"              .= nextProtocol'
      , "test_chain_status"          .= testChainStatus'
      , "max_operations_ttl"         .= maxOperationsTtl'
      , "max_operation_data_length" .= maxOperationsDataLength'
      , "max_block_header_length"    .= maxBlockHeaderLength'
      , "max_operation_list_length"  .= maxOperationListLength'
      , "baker"                      .= baker'
      , "level"                      .= level'
      , "voting_period_kind"         .= votingPeriodKind'
      , "nonce_hash"                 .= nonceHash'
      , "consumed_gas"               .= (StringEncode consumedGas')
      , "deactivated"                .= deactivated'
      , "balance_updates"            .= balanceUpdates'
      ]

instance FromJSON BlockHeaderMetadata where
  parseJSON = withObject "BlockHeaderMetadata" $ \o ->
    BlockHeaderMetadata <$> o .: "protocol"
                        <*> o .: "next_protocol"
                        <*> o .: "test_chain_status"
                        <*> o .: "max_operations_ttl"
                        <*> o .: "max_operation_data_length"
                        <*> o .: "max_block_header_length"
                        <*> o .: "max_operation_list_length"
                        <*> o .: "baker"
                        <*> o .: "level"
                        <*> o .: "voting_period_kind"
                        <*> o .: "nonce_hash"
                        <*> (fmap unStringEncode $ o .: "consumed_gas")
                        <*> o .: "deactivated"
                        <*> o .: "balance_updates"

data TestChainStatus
  = TCSNotRunning
  | TCSForking { protocol :: Text, expiration :: Timestamp }
  | TCSRunning { protocol :: Text, expiration :: Timestamp, chainId :: Text, genesis :: Text }
  deriving (Eq, Show, Generic)

instance ToJSON TestChainStatus where
  toJSON TCSNotRunning = object ["status" .= ("not_running" :: Text)]
  toJSON (TCSForking protocol' expiration') =
    object
      [ "status"     .= ("forking" :: Text)
      , "protocol"   .= protocol'
      , "expiration" .= expiration'
      ]
  toJSON (TCSRunning protocol' expiration' chainId' genesis') =
    object
      [ "status"     .= ("running" :: Text)
      , "protocol"   .= protocol'
      , "expiration" .= expiration'
      , "chain_id"   .= chainId'
      , "genesis"    .= genesis'
      ]
  
instance FromJSON TestChainStatus where
  parseJSON = withObject "TestChainStatus" $ \o ->
    case HM.lookup "status" o of
      Just "not_running" -> pure TCSNotRunning
      Just "forking"     ->
        TCSForking <$> o .: "protocol"
                   <*> o .: "expiration"
      Just "running"     ->
        TCSRunning <$> o .: "protocol"
                   <*> o .: "expiration"
                   <*> o .: "chain_id"
                   <*> o .: "genesis"
      _ -> fail "TestChainStatus expected object with key 'status'."

data MaxOperationLength =
  MaxOperationLength
    { maxSize :: Int64
    , maxOp   :: Maybe Int64
    } deriving (Eq, Show, Generic)

instance ToJSON MaxOperationLength where
  toJSON (MaxOperationLength maxSize' maxOp') =
    object $
      [ "max_size" .= maxSize'
      ] ++ catMaybes ["max_op" .=? maxOp']

instance FromJSON MaxOperationLength where
  parseJSON = withObject "MaxOperationLength" $ \o ->
    MaxOperationLength <$> o .:  "max_size"
                       <*> o .:? "max_op"

data BlockHeaderLevel =
  BlockHeaderLevel
    { level                :: Int64 -- no string encoding
    , levelPosition        :: Int64
    , cycle                :: Int64
    , cyclePosition        :: Int64
    , votingPeriod         :: Int64
    , votingPeriodPosition :: Int64
    , expectedCommitment   :: Bool
    } deriving (Eq, Show, Generic)

instance ToJSON BlockHeaderLevel where
  toJSON (BlockHeaderLevel level' levelPosition' cycle' cyclePosition'
          votingPeriod' votingPeriodPosition' expectedCommitment') =
    object
      [ "level"                  .= level'
      , "level_position"         .= levelPosition'
      , "cycle"                  .= cycle'
      , "cycle_position"         .= cyclePosition'
      , "voting_period"          .= votingPeriod'
      , "voting_period_position" .= votingPeriodPosition'
      , "expected_commitment"    .= expectedCommitment'
      ]

instance FromJSON BlockHeaderLevel where
  parseJSON = withObject "BlockHeaderLevel" $ \o ->
    BlockHeaderLevel <$> o .: "level"
                     <*> o .: "level_position"
                     <*> o .: "cycle"
                     <*> o .: "cycle_position"
                     <*> o .: "voting_period"
                     <*> o .: "voting_period_position"
                     <*> o .: "expected_commitment"

data BalanceUpdate
  = BalanceUpdateContract { contract :: Text , change :: Integer } -- ContractId
  | FreezerRewards        { delegate :: Text, cycle :: Int64, change :: Integer }
  | FreezerFees           { delegate :: Text, cycle :: Int64, change :: Integer }
  | FreezerDeposits       { delegate :: Text, cycle :: Int64, change :: Integer }
  deriving (Eq, Show, Generic)

instance ToJSON BalanceUpdate where
  toJSON (BalanceUpdateContract contract' change') =
    object
      [ "kind"     .= ("contract" :: Text)
      , "contract" .= contract'
      , "change"   .= (StringEncode change')
      ]
  toJSON (FreezerRewards delegate' cycle' change') =
    object
      [ "kind"     .= ("freezer" :: Text)
      , "category" .= ("rewards"  :: Text)
      , "delegate" .= delegate'
      , "cycle"    .= cycle'
      , "change"   .= (StringEncode change')
      ]
  toJSON (FreezerFees delegate' cycle' change') =
    object
      [ "kind"     .= ("freezer" :: Text)
      , "category" .= ("fees"     :: Text)
      , "delegate" .= delegate'
      , "cycle"    .= cycle'
      , "change"   .= (StringEncode change')
      ]
  toJSON (FreezerDeposits delegate' cycle' change') =
    object
      [ "kind"     .= ("freezer" :: Text)
      , "category" .= ("deposits" :: Text)
      , "delegate" .= delegate'
      , "cycle"    .= cycle'
      , "change"   .= (StringEncode change')
      ]

instance FromJSON BalanceUpdate where
  parseJSON = withObject "BalanceUpdate" $ \o ->
    case HM.lookup "kind" o of
      Just "contract" ->
        BalanceUpdateContract <$> o .: "contract"
                              <*> (unStringEncode <$> o .: "change")
      Just "freezer" ->
        case HM.lookup "category" o of
          Just "rewards"  ->
            FreezerRewards <$> o .: "delegate"
                           <*> o .: "cycle"
                           <*> (unStringEncode <$> o .: "change")
          Just "fees"     ->
            FreezerFees <$> o .: "delegate"
                        <*> o .: "cycle"
                        <*> (unStringEncode <$> o .: "change")
          Just "deposits" ->
            FreezerDeposits <$> o .: "delegate"
                            <*> o .: "cycle"
                            <*> (unStringEncode <$> o .: "change")
          _ -> fail "Expected key 'category' in with 'kind' of 'freezer'."
      _ -> fail "Expected key 'kind'."


newtype BalanceUpdates
  = BalanceUpdates { balanceUpdates :: [BalanceUpdate] }
  deriving (Eq, Show, Generic)

instance ToJSON BalanceUpdates where
  toJSON (BalanceUpdates balanceUpdates') =
    object
      [ "balance_updates" .= balanceUpdates'
      ]

instance FromJSON BalanceUpdates where
  parseJSON = withObject "BalanceUpdates" $ \o ->
    BalanceUpdates <$> o .: "balance_updates"

data OperationAndResult
  = OAR 
    { protocol          :: Text
    , chainId           :: Text
    , hash              :: Text
    , branch            :: Text
    , contentsAndResult :: [OperationContentsAndResult]
    , signature         :: Maybe Text
    } deriving (Eq, Show, Generic)

instance ToJSON OperationAndResult where
  toJSON (OAR protocol' chainId' hash' branch' contentsAndResult' signature') =
    object $
      [ "protocol"  .= protocol'
      , "chain_id"  .= chainId'
      , "hash"      .= hash'
      , "branch"    .= branch'
      , "contents"  .= contentsAndResult'
      ] ++ catMaybes ["signature" .=? signature']

instance FromJSON OperationAndResult where
  parseJSON = withObject "OperationAndResult" $ \o ->
    OAR <$> o .:  "protocol"
        <*> o .:  "chain_id"
        <*> o .:  "hash"
        <*> o .:  "branch"
        <*> o .:  "contents"
        <*> o .:? "signature"
  
data OperationWithoutResult
  = OWR
    { protocol          :: Text
    , chainId           :: Text
    , hash              :: Text
    , branch            :: Text
    , contents          :: [OperationContents]
    , signature         :: Maybe Text
    } deriving (Eq, Show, Generic)

instance ToJSON OperationWithoutResult where
  toJSON (OWR protocol' chainId' hash' branch' contents' signature') =
    object $
      [ "protocol"  .= protocol'
      , "chain_id"  .= chainId'
      , "hash"      .= hash'
      , "branch"    .= branch'
      , "contents"  .= contents'
      ] ++ catMaybes ["signature" .=? signature']

instance FromJSON OperationWithoutResult where
  parseJSON = withObject "Operation" $ \o ->
    OWR <$> o .:  "protocol"
        <*> o .:  "chain_id"
        <*> o .:  "hash"
        <*> o .:  "branch"
        <*> o .:  "contents"
        <*> o .:? "signature"

data Operation
 = OperationAndResult OperationAndResult
 | Operation OperationWithoutResult
 deriving (Eq, Show, Generic)

instance ToJSON Operation where
  toJSON (OperationAndResult o) = toJSON o
  toJSON (Operation o) = toJSON o

instance FromJSON Operation where
  parseJSON = withObject "Operation" $ \o -> do
    case HM.lookup "contents" o of
      Just (Array contents') ->
        if V.length contents' == 0
          then Operation <$> parseJSON (Object o)
          else
            case V.head contents' of
              Object content' ->
                if HM.member "metadata" content'
                  then OperationAndResult <$> parseJSON (Object o)            
                  else Operation <$> parseJSON (Object o)
              _ -> fail "parseJSON Operation expected a the objects of key 'contents' to be operations."
      _ -> fail "parseJSON Operation expected a key 'contents' of an array."

data OperationSeedNonceRevelation =
  OperationSeedNonceRevelation
    { level :: Int64
    , nonce :: Text
    } deriving (Eq, Show, Generic)

instance ToJSON OperationSeedNonceRevelation where
  toJSON (OperationSeedNonceRevelation level' nonce') =
    object
      [ "kind"  .= ("seed_nonce_revelation" :: Text)
      , "level" .= level'
      , "nonce" .= nonce'
      ]
    
instance FromJSON OperationSeedNonceRevelation where
  parseJSON = withObject "OperationSeedNonceRevelation" $ \o ->
    OperationSeedNonceRevelation <$> o .: "level" <*> o .: "nonce"

data OperationDoubleEndorsementEvidence =
  OperationDoubleEndorsementEvidence
    { op1 :: Endorsement
    , op2 :: Endorsement
    } deriving (Eq, Show, Generic)

instance ToJSON OperationDoubleEndorsementEvidence where
  toJSON (OperationDoubleEndorsementEvidence op1' op2') =      
    object ["op1" .= op1', "op2" .= op2']    

instance FromJSON OperationDoubleEndorsementEvidence where
  parseJSON = withObject "OperationDoubleEndorsementEvidence" $ \o ->
    OperationDoubleEndorsementEvidence <$> o .: "op1" <*> o .: "op2"

data OperationDoubleBakingEvidence =
  OperationDoubleBakingEvidence
    { bh1 :: FullHeader
    , bh2 :: FullHeader
    } deriving (Eq, Show, Generic)

instance ToJSON OperationDoubleBakingEvidence where
  toJSON (OperationDoubleBakingEvidence bh1' bh2') =
    object ["bh1" .= bh1', "bh2" .= bh2']

instance FromJSON OperationDoubleBakingEvidence where
  parseJSON = withObject "OperationDoubleBakingEvidence" $ \o ->
    OperationDoubleBakingEvidence <$> o .: "bh1" <*> o .: "bh2"

data OperationActivateAccount
 = OperationActivateAccount
    { pkh    :: Text
    , secret :: Text
    } deriving (Eq, Show, Generic)

instance ToJSON OperationActivateAccount where
  toJSON (OperationActivateAccount pkh' secret') =
    object
      [ "pkh"    .= pkh'
      , "secret" .= secret'
      ]

instance FromJSON OperationActivateAccount where
  parseJSON = withObject "OperationActivateAccount" $ \o ->
    OperationActivateAccount <$> o .: "pkh" <*> o .: "secret"

data OperationProposals =
  OperationProposals
    { sourcePkhs :: Text
    , period     :: Int64
    , proposals  :: [Text]
    } deriving (Eq, Show, Generic)

instance ToJSON OperationProposals where
  toJSON (OperationProposals sourcePkhs' period' proposals') =
    object
      [ "source"    .= sourcePkhs'
      , "period"    .= period'
      , "proposals" .= proposals'
      ]

instance FromJSON OperationProposals where
  parseJSON = withObject "OperationProposals" $ \o ->
    OperationProposals
    <$> o .: "source"
    <*> o .: "period"
    <*> o .: "proposals"

data OperationBallot =
  OperationBallot
    { source        :: Text -- ContractId
    , period        :: Int64
    , proposal      :: Text
    , ballot        :: Ballot
    } deriving (Eq, Show, Generic)

instance ToJSON OperationBallot where
  toJSON (OperationBallot source' period' proposal' ballot') =
    object      
      [ "source"    .= source'
      , "period"    .= period'
      , "proposal"  .= proposal'
      , "ballot"    .= ballot'
      ]

instance FromJSON OperationBallot where
  parseJSON = withObject "OperationBallot" $ \o ->
    OperationBallot
    <$> o .: "source"
    <*> o .: "period"
    <*> o .: "proposal"
    <*> o .: "ballot"

data OperationReveal =
  OperationReveal
    { source        :: Text -- ContractId
    , fee           :: Mutez
    , counter       :: Natural -- PositiveBignum
    , gasLimit      :: Natural -- PositiveBignum
    , storageLimit  :: Natural -- PositiveBignum
    , publicKey     :: Text
    } deriving (Eq, Show, Generic)

instance ToJSON OperationReveal where
  toJSON (OperationReveal source' fee' counter' gasLimit'
          storageLimit' publicKey') =
    object
      [ "source"        .= source'
      , "fee"           .= fee'
      , "counter"       .= (StringEncode counter')
      , "gas_limit"     .= (StringEncode gasLimit')
      , "storage_limit" .= (StringEncode storageLimit')
      , "public_key"    .= publicKey'
      ]
    
instance FromJSON OperationReveal where
  parseJSON = withObject "OperationReveal" $ \o ->
    OperationReveal
    <$> o .: "source"
    <*> o .: "fee"
    <*> (fmap unStringEncode $ o .: "counter")
    <*> (fmap unStringEncode $ o .: "gas_limit")
    <*>  (fmap unStringEncode $ o .: "storage_limit")
    <*> o .: "public_key"    

data OperationTransaction =
  OperationTransaction
    { source              :: Text -- ContractId
    , fee                 :: Mutez
    , counter             :: Natural -- PositiveBignum
    , gasLimit            :: Natural -- PositiveBignum
    , storageLimit        :: Natural -- PositiveBignum
    , amount              :: Mutez
    , destination         :: Text -- ContractId
    , parameters          :: Maybe Parameters
    } deriving (Eq, Show, Generic)

instance ToJSON OperationTransaction where
  toJSON (OperationTransaction source' fee' counter' gasLimit'
          storageLimit' amount' destination' parameters') =
    object $
      [ "source"        .= source'
      , "fee"           .= fee'
      , "counter"       .= (StringEncode counter')
      , "gas_limit"     .= (StringEncode gasLimit')
      , "storage_limit" .= (StringEncode storageLimit')
      , "amount"        .= amount'
      , "destination"   .= destination'
      ] ++ catMaybes ["parameters" .=? parameters']

instance FromJSON OperationTransaction where
  parseJSON = withObject "OperationTransaction" $ \o ->
    OperationTransaction <$> o .:  "source"
                         <*> o .:  "fee"
                         <*> (fmap unStringEncode $ o .:  "counter")
                         <*> (fmap unStringEncode $ o .:  "gas_limit")
                         <*> (fmap unStringEncode $ o .:  "storage_limit")
                         <*> o .:  "amount"
                         <*> o .:  "destination"
                         <*> o .:? "parameters"

data OperationOrigination =
  OperationOrigination
    { source        :: Text -- ContractId
    , fee           :: Mutez
    , counter       :: Natural -- PositiveBignum
    , gasLimit      :: Natural -- PositiveBignum
    , storageLimit  :: Natural -- PositiveBignum
    , balance       :: Mutez
    , delegate      :: Maybe Text
    , script        :: Contract
    } deriving (Eq, Show, Generic)

instance ToJSON OperationOrigination where
  toJSON (OperationOrigination source' fee' counter' gasLimit'
          storageLimit' balance' delegate' script') =
    object $
      [ "source"         .= source'
      , "fee"            .= fee'
      , "counter"        .= StringEncode counter'
      , "gas_limit"      .= StringEncode gasLimit'
      , "storage_limit"  .= StringEncode storageLimit'
      , "balance"        .= balance'
      , "script"         .= script'      
      ] ++ catMaybes
      [ "delegate"      .=? delegate'
      ]

instance FromJSON OperationOrigination where
  parseJSON = withObject "OperationOrigination" $ \o -> OperationOrigination
    <$> o .:  "source"
    <*> o .:  "fee"
    <*> (fmap unStringEncode $ o .:  "counter")
    <*> (fmap unStringEncode $ o .:  "gas_limit")
    <*> (fmap unStringEncode $ o .:  "storage_limit")
    <*> o .:  "balance"
    <*> o .:? "delegate"
    <*> o .:  "script"

data OperationDelegation =
  OperationDelegation
    { source       :: Text -- ContractId
    , fee          :: Mutez
    , counter      :: Natural -- PositiveBignum
    , gasLimit     :: Natural -- PositiveBignum
    , storageLimit :: Natural -- PositiveBignum
    , delegate     :: Maybe Text
    } deriving (Eq, Show, Generic)

instance ToJSON OperationDelegation where
  toJSON (OperationDelegation source' fee' counter' gasLimit' storageLimit'
          delegate') =
    object $
      [ "source"         .= source'
      , "fee"            .= fee'
      , "counter"        .= StringEncode counter'
      , "gas_limit"      .= StringEncode gasLimit'
      , "storage_limit"  .= StringEncode storageLimit'
      ] ++ catMaybes 
      [ "delegate"       .=? delegate'
      ]

instance FromJSON OperationDelegation where
  parseJSON = withObject "OperationDelegation" $ \o ->
    OperationDelegation
    <$> o .:  "source"
    <*> o .:  "fee"
    <*> (fmap unStringEncode $ o .:  "counter")
    <*> (fmap unStringEncode $ o .:  "gas_limit")
    <*> (fmap unStringEncode $ o .:  "storage_limit")
    <*> o .:? "delegate"

data OperationContents
  = OperationContentsEndorsement {level :: Int64}  
  | OperationContentsSeedNonceRevelation OperationSeedNonceRevelation
  | OperationContentsDoubleEndorsementEvidence OperationDoubleEndorsementEvidence
  | OperationContentsDoubleBakingEvidence OperationDoubleBakingEvidence
  | OperationContentsActivateAccount OperationActivateAccount 
  | OperationContentsProposals OperationProposals
  | OperationContentsBallot OperationBallot
  | OperationContentsReveal OperationReveal
  | OperationContentsTransaction OperationTransaction
  | OperationContentsOrigination OperationOrigination
  | OperationContentsDelegation OperationDelegation
  deriving (Eq, Show, Generic)

instance ToJSON OperationContents where
  toJSON (OperationContentsEndorsement level') =
    object
      [ "kind"  .= ("endorsement" :: Text)
      , "level" .= level'
      ]

  toJSON (OperationContentsSeedNonceRevelation revelation') =
    combineValues
    (object ["kind" .= ("seed_nonce_revelation" :: Text)])
    (toJSON revelation')
    
  toJSON (OperationContentsDoubleEndorsementEvidence evidence') =
    combineValues
    (object ["kind" .= ("double_endorsement_evidence" :: Text)])
    (toJSON evidence')

  toJSON (OperationContentsDoubleBakingEvidence doubleBaking') =
    combineValues
    (object ["kind" .= ("double_baking_evidence" :: Text)])
    (toJSON doubleBaking')

  toJSON (OperationContentsActivateAccount activateAccount') =
    combineValues
    (object ["kind" .= ("activate_account" :: Text)])
    (toJSON activateAccount')

  toJSON (OperationContentsProposals proposals') =
    combineValues (object ["kind" .= ("proposals" :: Text)]) (toJSON proposals')

  toJSON (OperationContentsBallot ballot') =
    combineValues (object ["kind" .= ("ballot" :: Text)]) (toJSON ballot')

  toJSON (OperationContentsReveal reveal') =
    combineValues (object ["kind" .= ("reveal" :: Text)])
    (toJSON reveal')

  toJSON (OperationContentsTransaction transaction') =
    combineValues (object ["kind" .= ("transaction" :: Text)])
    (toJSON transaction')
    
  toJSON (OperationContentsOrigination origination') =
    combineValues (object [ "kind" .= ("origination" :: Text)]) (toJSON origination')

  toJSON (OperationContentsDelegation delegation') =
    combineValues (object ["kind" .= ("delegation" :: Text)]) (toJSON delegation')

instance FromJSON OperationContents where
  parseJSON = withObject "OperationContents" $ \o ->
    case HM.lookup "kind" o of
      Just "endorsement" ->
        OperationContentsEndorsement <$> o .: "level"

      Just "seed_nonce_revelation" ->
        OperationContentsSeedNonceRevelation  <$> parseJSON (Object o)

      Just "double_endorsement_evidence" ->
        OperationContentsDoubleEndorsementEvidence <$> parseJSON (Object o)

      Just "double_baking_evidence" ->
        OperationContentsDoubleBakingEvidence <$> parseJSON (Object o)

      Just "activate_account" ->
        OperationContentsActivateAccount <$> parseJSON (Object o)

      Just "proposals" ->
        OperationContentsProposals <$> parseJSON (Object o)

      Just "ballot" ->
        OperationContentsBallot <$> parseJSON (Object o)

      Just "reveal" ->
        OperationContentsReveal <$> parseJSON (Object o) 

      Just "transaction" ->
        OperationContentsTransaction <$> parseJSON (Object o)

      Just "origination" ->
        OperationContentsOrigination <$> parseJSON (Object o)

      Just "delegation" ->
        OperationContentsDelegation <$> parseJSON (Object o)

      _ -> fail "OperationContents expected a key 'kind'."

data OperationContentsAndResult
  = OperationContentsAndResultEndorsement
    { level    :: Int64
    , metadataEndorsement :: OperationEndorsementMetadata
    }  
  | OperationContentsAndResultSeedNonceRevelation OperationSeedNonceRevelation BalanceUpdates
  | OperationContentsAndResultDoubleEndorsementEvidence OperationDoubleEndorsementEvidence BalanceUpdates
  | OperationContentsAndResultDoubleBakingEvidence OperationDoubleBakingEvidence BalanceUpdates
  | OperationContentsAndResultActivateAccount OperationActivateAccount BalanceUpdates
  | OperationContentsAndResultProposals OperationProposals
  | OperationContentsAndResultBallot OperationBallot
  | OperationContentsAndResultReveal OperationReveal (InternalOperationResultsMetadata Reveal)
  | OperationContentsAndResultTransaction OperationTransaction (InternalOperationResultsMetadata Transaction)
  | OperationContentsAndResultOrigination OperationOrigination (InternalOperationResultsMetadata Origination)
  | OperationContentsAndResultDelegation OperationDelegation (InternalOperationResultsMetadata Delegation)
  deriving (Eq, Show, Generic)

instance ToJSON OperationContentsAndResult where
  toJSON (OperationContentsAndResultEndorsement level' metadataEndorsement') =
    object
      [ "kind"     .= ("endorsement" :: Text)
      , "level"    .= level'
      , "metadata" .= metadataEndorsement'
      ]

  toJSON (OperationContentsAndResultSeedNonceRevelation seedNonceRevelation' metadata') =
    combineValues
    (object ["kind" .= ("seed_nonce_revelation" :: Text), "metadata" .= metadata'])
    (toJSON seedNonceRevelation')

  toJSON (OperationContentsAndResultDoubleEndorsementEvidence doubleEndorsement' metadata') =
    combineValues
    (object ["kind" .= ("double_endorsement_evidence" :: Text),"metadata" .= metadata'])
    (toJSON doubleEndorsement')

  toJSON (OperationContentsAndResultDoubleBakingEvidence doubleBaking' metadata') =
    combineValues
    (object ["kind" .= ("double_baking_evidence" :: Text), "metadata" .= metadata'])
    (toJSON doubleBaking')

  toJSON (OperationContentsAndResultActivateAccount activateAccount' metadata') =
    combineValues
    (object ["kind" .= ("activate_account" :: Text), "metadata" .= metadata'])
    (toJSON activateAccount')

  toJSON (OperationContentsAndResultProposals proposals') =
    combineValues
    (object ["kind" .= ("proposals" :: Text), "metadata" .= (Object HM.empty)])
    (toJSON proposals')    

  toJSON (OperationContentsAndResultBallot ballot') =
    combineValues
    (object ["kind" .= ("ballot" :: Text), "metadata"  .= (Object HM.empty)])
    (toJSON ballot')

  toJSON (OperationContentsAndResultReveal reveal' metadata') =
    combineValues
    (object ["kind" .= ("reveal" :: Text), "metadata" .= metadata'])
    (toJSON reveal')

  toJSON (OperationContentsAndResultTransaction transaction' metadata') =
    combineValues
    (object ["kind" .= ("transaction" :: Text), "metadata" .= metadata'])
    (toJSON transaction')
    
  toJSON (OperationContentsAndResultOrigination origination' metadata') =
    combineValues
    (object ["kind" .= ("origination" :: Text), "metadata" .= metadata'])
    (toJSON origination')

  toJSON (OperationContentsAndResultDelegation delegation' metadata') =
    combineValues
    (object [ "kind" .= ("delegation" :: Text), "metadata" .= metadata'])
    (toJSON delegation')

instance FromJSON OperationContentsAndResult where
  parseJSON = withObject "OperationContentsAndResult" $ \o ->
    case HM.lookup "kind" o of
      Just "endorsement" ->
        OperationContentsAndResultEndorsement
        <$> o .: "level"
        <*> o .: "metadata"

      Just "seed_nonce_revelation" ->
        OperationContentsAndResultSeedNonceRevelation
        <$> parseJSON (Object o)
        <*> o .: "metadata"

      Just "double_endorsement_evidence" ->
        OperationContentsAndResultDoubleEndorsementEvidence
        <$> parseJSON (Object o)
        <*> o .: "metadata"

      Just "double_baking_evidence" ->
        OperationContentsAndResultDoubleBakingEvidence
        <$> parseJSON (Object o)
        <*> o .: "metadata"

      Just "activate_account" ->
        OperationContentsAndResultActivateAccount
        <$> parseJSON (Object o)
        <*> o .: "metadata"

      Just "proposals" ->
        OperationContentsAndResultProposals
        <$> parseJSON (Object o)

      Just "ballot" ->
        OperationContentsAndResultBallot
        <$> parseJSON (Object o)

      Just "reveal" ->
        OperationContentsAndResultReveal
        <$> parseJSON (Object o)
        <*> o .: "metadata"

      Just "transaction" ->
        OperationContentsAndResultTransaction
        <$> parseJSON (Object o)
        <*> o .: "metadata"

      Just "origination" ->
        OperationContentsAndResultOrigination
        <$> parseJSON (Object o)
        <*> o .: "metadata"

      Just "delegation" ->
        OperationContentsAndResultDelegation
        <$> parseJSON (Object o)
        <*> o .: "metadata"

      _ -> fail "OperationContentsAndResult expected a key 'kind'."

data OperationEndorsementMetadata =
  OperationEndorsementMetadata
    { balanceUpdates :: [BalanceUpdate]
    , delegate       :: Text
    , slots          :: [Word8]
    } deriving (Eq, Show, Generic)

instance ToJSON OperationEndorsementMetadata where
  toJSON (OperationEndorsementMetadata balanceUpdates' delegate' slots') =
    object
      [ "balance_updates" .= balanceUpdates'
      , "delegate"        .= delegate'
      , "slots"           .= slots'
      ]

instance FromJSON OperationEndorsementMetadata where
  parseJSON = withObject "OperationEndorsementMetadata" $ \o ->
    OperationEndorsementMetadata <$> o .: "balance_updates"
                                 <*> o .: "delegate"
                                 <*> o .: "slots"

data InternalOperationResultsMetadata a =
  InternalOperationResultsMetadata
    { balanceUpdates           :: [BalanceUpdate]
    , operationResult          :: a
    , internalOperationResults :: Maybe [InternalOperationResult]
    } deriving (Eq, Show, Generic)

instance (ToJSON a) => ToJSON (InternalOperationResultsMetadata a) where
  toJSON (InternalOperationResultsMetadata balanceUpdates' operationResult'
          internalOperationResults') =
    object $
      [ "balance_updates"           .= balanceUpdates'
      , "operation_result"          .= operationResult'
      ] ++ catMaybes 
      [ "internal_operation_results" .=? internalOperationResults'
      ]
  
instance (FromJSON a) => FromJSON (InternalOperationResultsMetadata a) where
  parseJSON = withObject "InternalOperationResultsMetadata" $ \o ->
    InternalOperationResultsMetadata <$> o .:  "balance_updates"
                                     <*> o .:  "operation_result"
                                     <*> o .:? "internal_operation_results"

data RawBlockHeader =
  RawBlockHeader
    { level            :: Int64
    , proto            :: Word8
    , predecessor      :: Text
    , timestamp        :: Timestamp
    , validationPass   :: Word8
    , operationsHash   :: Text
    , fitness          :: [Text]
    , context          :: Text
    , priority         :: Word16
    , proofOfWorkNonce :: Maybe Text
    , signature        :: Text
    } deriving (Eq, Show, Generic)

instance ToJSON RawBlockHeader where
  toJSON (RawBlockHeader level' proto' predecessor' timestamp' validationPass'
         operationsHash' fitness' context' priority' proofOfWorkNonce'
         signature') =
    object $
      [ "level"               .= level'
      , "proto"               .= proto'
      , "predecessor"         .= predecessor'
      , "timestamp"           .= timestamp'
      , "validation_pass"     .= validationPass'
      , "operations_hash"     .= operationsHash'
      , "fitness"             .= fitness'
      , "context"             .= context'
      , "priority"            .= priority'
      , "signature"           .= signature'
      ] ++ catMaybes ["proof_of_work_nonce" .=? proofOfWorkNonce']

instance FromJSON RawBlockHeader where
  parseJSON = withObject "RawBlockHeader" $ \o ->
    RawBlockHeader <$> o .:  "level"
                   <*> o .:  "proto"
                   <*> o .:  "predecessor"
                   <*> o .:  "timestamp"
                   <*> o .:  "validation_pass"
                   <*> o .:  "operations_hash"
                   <*> o .:  "fitness"
                   <*> o .:  "context"
                   <*> o .:  "priority"
                   <*> o .:? "proof_of_work_nonce"
                   <*> o .:  "signature"

data InternalOperationResult
  = InternalOperationResultReveal
    { source       :: Text -- ContractId
    , nonce        :: Word16
    , publicKey    :: Text
    , resultReveal :: Reveal
    }
  | InternalOperationResultTransaction
    { source            :: Text -- ContractId
    , nonce             :: Word16
    , amount            :: Mutez
    , destination       :: Text -- ContractId
    , parameters        :: Maybe Parameters
    , resultTransaction :: Transaction
    }
  | InternalOperationResultOrigination
    { source            :: Text -- ContractId
    , nonce             :: Word16
    , balance           :: Mutez
    , delegate          :: Maybe Text
    , script            :: Contract
    , resultOrigination :: Origination
    }
  | InternalOperationResultDelegation
    { source           :: Text -- ContractId
    , nonce            :: Word16
    , delegate         :: Maybe Text
    , resultDelegation :: Delegation
    }
  deriving (Eq, Show, Generic)

instance ToJSON InternalOperationResult where
  toJSON (InternalOperationResultReveal source' nonce' publicKey'
          resultReveal') =
    object
      [ "kind"     .= ("reveal" :: Text)
      , "source"     .= source'
      , "nonce"      .= nonce'
      , "public_key" .= publicKey'
      , "result"     .= resultReveal'
      ]
  toJSON (InternalOperationResultTransaction source' nonce' amount'
          destination' parameters' resultTransaction') =
    object $
      [ "kind"     .= ("transaction" :: Text)
      , "source"      .= source'
      , "nonce"       .= nonce'
      , "amount"      .= amount'
      , "destination" .= destination'
      , "result"      .= resultTransaction'
      ] ++ catMaybes ["parameters"  .=? parameters']
  toJSON (InternalOperationResultOrigination source' nonce' balance' delegate'
          script' resultOrigination') =
    object $
      [ "kind"     .= ("origination" :: Text)
      , "source"   .= source'
      , "nonce"    .= nonce'
      , "balance"  .= balance'
      , "script"   .= script'
      , "result"   .= resultOrigination'
      ] ++ catMaybes ["delegate" .=? delegate']
  toJSON (InternalOperationResultDelegation source' nonce' delegate'
          resultDelegate') =
    object $
      [ "kind"     .= ("delegation" :: Text)
      , "source"   .= source'
      , "nonce"    .= nonce'
      , "result"   .= resultDelegate'
      ] ++ catMaybes ["delegate" .=? delegate']
  
instance FromJSON InternalOperationResult where
  parseJSON = withObject "InternalOperationResult" $ \o ->
    case HM.lookup "kind" o of
      Just "reveal"      ->
        InternalOperationResultReveal <$> o .: "source"
                                      <*> o .: "nonce"
                                      <*> o .: "public_key"
                                      <*> o .: "result"
      Just "transaction" ->
        InternalOperationResultTransaction <$> o .:  "source"
                                           <*> o .:  "nonce"
                                           <*> o .:  "amount"
                                           <*> o .:  "destination"
                                           <*> o .:? "parameters"
                                           <*> o .:  "result"
      Just "origination" ->
        InternalOperationResultOrigination <$> o .:  "source"
                                           <*> o .:  "nonce"
                                           <*> o .:  "balance"
                                           <*> o .:? "delegate"
                                           <*> o .:  "script"
                                           <*> o .:  "result"
      Just "delegation"  ->
        InternalOperationResultDelegation <$> o .:  "source"
                                          <*> o .:  "nonce"
                                          <*> o .:? "delegate"
                                          <*> o .:  "result"
      _ -> fail "InternalOperationResult expected a key 'kind'."

data Delegation
  = DelegationApplied
    { consumedGas :: Maybe Integer -- Bignum
    , consumedMilligas :: Maybe Integer -- Bignum
    }
  | DelegationFailed
    { errors :: [Error] }
  | DelegationSkipped
  | DelegationBacktracked
    { consumedGas :: Maybe Integer -- Bignum
    , consumedMilligas :: Maybe Integer -- Bignum
    , mErrors     :: Maybe [Error]
    }
  deriving (Eq, Show, Generic)

instance ToJSON Delegation where
  toJSON (DelegationApplied consumedGas' consumedMilligas') =
    object $
      [ "status"         .= ("applied" :: Text )
      ] ++ catMaybes ["consumed_gas" .=? (StringEncode <$> consumedGas'), "consumed_milligas" .=? (StringEncode <$> consumedMilligas')]
  toJSON (DelegationFailed errors') =
    object
      [ "status"   .= ("failed" :: Text )
      , "errors" .= errors'
      ]
  toJSON DelegationSkipped =
    object
      [ "status" .= ("skipped" :: Text )
      ]
  toJSON (DelegationBacktracked consumedGas' consumedMilligas' mErrors') =
    object $
      [ "status"         .=  ("backtracked" :: Text )
      ] ++ catMaybes
      [ "consumed_gas" .=? (StringEncode <$> consumedGas')
      , "consumed_milligas" .=? (StringEncode <$> consumedMilligas')
      , "errors"       .=? mErrors'
      ]
    
instance FromJSON Delegation where
  parseJSON = withObject "Delegation" $ \o ->
    case HM.lookup "status" o of
      Just "applied"    ->
        DelegationApplied <$>
        (fmap unStringEncode <$> o .:? "consumed_gas") <*>
        (fmap unStringEncode <$> o .:? "consumed_milligas")
      Just "failed"     ->
        DelegationFailed <$> o .: "errors"
      Just "skipped"    -> pure DelegationSkipped
      Just "backtracked" ->
        DelegationBacktracked <$> (fmap unStringEncode <$> o .:? "consumed_gas")
                              <*> (fmap unStringEncode <$> o .:? "consumed_milligas")
                              <*> o .:? "errors"
      _ -> fail "Delegation expected a key 'status'."
    
data Origination
  = OriginationApplied
      { bigMapDiff          :: Maybe [BigMapDiff]
      , balanceUpdates      :: Maybe [BalanceUpdate]
      , originatedContracts :: Maybe [Text] -- [ContractId]
      , consumedGas         :: Maybe Integer -- Bignum
      , consumedMilligas    :: Maybe Integer -- Bignum
      , storageSize         :: Maybe Integer -- Bignum
      , paidStorageSizeDiff :: Maybe Integer -- Bignum
      }
  | OriginationFailed
      { errors :: [Error] }
  | OriginationSkipped
  | OriginationBacktracked
      { mErrors             :: Maybe [Error]
      , bigMapDiff          :: Maybe [BigMapDiff]
      , balanceUpdates      :: Maybe [BalanceUpdate]
      , originatedContracts :: Maybe [Text] -- ContractId]
      , consumedGas         :: Maybe Integer -- Bignum
      , consumedMilligas    :: Maybe Integer -- Bignum      
      , storageSize         :: Maybe Integer -- Bignum
      , paidStorageSizeDiff :: Maybe Integer -- Bignum
      }
  deriving (Eq, Show, Generic)

instance ToJSON Origination where
  toJSON (OriginationApplied bigMapDiff' balanceUpdates' originatedContracts'
          consumedGas' consumedMilligas' storageSize' paidStorageSizeDiff') =
    object $
      [ "status"                   .=  ("applied" :: Text )
      ] ++ catMaybes
      [ "big_map_diff"           .=? bigMapDiff'
      , "balance_updates"        .=? balanceUpdates'
      , "originated_contracts"   .=? originatedContracts'
      , "consumed_gas"           .=? (fmap StringEncode consumedGas')
      , "consumed_milligas"      .=? (fmap StringEncode consumedMilligas')
      , "storage_size"           .=? (fmap StringEncode storageSize')
      , "paid_storage_size_diff" .=? (fmap StringEncode paidStorageSizeDiff')
      ]
  toJSON (OriginationFailed errors') =
    object
      [ "status"   .= ("failed" :: Text )
      , "errors" .= errors'
      ]
  toJSON OriginationSkipped =
    object
      [ "status" .= ("skipped" :: Text )
      ]
  toJSON (OriginationBacktracked mErrors' bigMapDiff' balanceUpdates'
          originatedContracts' consumedGas' consumedMilligas' storageSize' paidStorageSizeDiff') =
    object $
      [ "status"                   .=  ("backtracked" :: Text )
      ] ++ catMaybes
      [ "errors"                 .=? mErrors'
      , "big_map_diff"           .=? bigMapDiff'
      , "balance_updates"        .=? balanceUpdates'
      , "originated_contracts"   .=? originatedContracts'
      , "consumed_gas"           .=? (fmap StringEncode consumedGas')
      , "consumed_milligas"      .=? (fmap StringEncode consumedMilligas')
      , "storage_size"           .=? (fmap StringEncode storageSize')
      , "paid_storage_size_diff" .=? (fmap StringEncode paidStorageSizeDiff')
      ]
  
instance FromJSON Origination where
  parseJSON = withObject "Origination" $ \o ->
    case HM.lookup "status" o of
      Just "applied"    ->
        OriginationApplied <$> o .:? "big_map_diff"
                           <*> o .:? "balance_updates"
                           <*> o .:? "originated_contracts"
                           <*> (fmap unStringEncode <$> o .:? "consumed_gas")
                           <*> (fmap unStringEncode <$> o .:? "consumed_milligas")
                           <*> (fmap unStringEncode <$> o .:? "storage_size")
                           <*> (fmap unStringEncode <$> o .:? "paid_storage_size_diff")
      Just "failed"     ->
        OriginationFailed <$> o .: "errors"
      Just "skipped"    -> pure OriginationSkipped
      Just "backtracked" ->
        OriginationBacktracked <$> o .:? "errors"
                               <*> o .:? "big_map_diff"
                               <*> o .:? "balance_updates"
                               <*> o .:? "originated_contracts"
                               <*> (fmap unStringEncode <$> o .:? "consumed_gas")
                               <*> (fmap unStringEncode <$> o .:? "consumed_milligas")
                               <*> (fmap unStringEncode <$> o .:? "storage_size")
                               <*> (fmap unStringEncode <$> o .:? "paid_storage_size_diff")
      _ -> fail "Origination expected a key 'status'."

data Reveal
  = RevealApplied
      { consumedGas      :: Maybe Integer -- Bignum
      , consumedMilligas :: Maybe Integer -- Bignum
      }
  | RevealFailed
      { errors :: [Error] }
  | RevealSkipped
  | RevealBacktracked
      { consumedGas      :: Maybe Integer -- Bignum
      , consumedMilligas :: Maybe Integer -- Bignum      
      , mErrors          :: Maybe [Error]
      }
  deriving (Eq, Show, Generic)

instance ToJSON Reveal where
  toJSON (RevealApplied consumedGas' consumedMilligas') =
    object $
      [ "status"       .=  ("applied" :: Text )
      ] ++ catMaybes
      [ "consumed_gas"      .=? (fmap StringEncode consumedGas')
      , "consumed_milligas" .=? (fmap StringEncode consumedMilligas')
      ]
  toJSON (RevealFailed errors') =
    object
      [ "status" .= ("failed" :: Text )
      , "errors" .= errors'
      ]
  toJSON RevealSkipped =
    object
      [ "status" .= ("skipped" :: Text )
      ]
  toJSON (RevealBacktracked consumedGas' consumedMilligas' mErrors') =
    object $
      [ "status"       .= ("backtracked" :: Text )
      ] ++ catMaybes 
      [ "consumed_gas"      .=? (fmap StringEncode consumedGas')
      , "consumed_milligas" .=? (fmap StringEncode consumedMilligas')
      , "errors"            .=? mErrors'
      ]

instance FromJSON Reveal where
  parseJSON = withObject "Reveal" $ \o ->
    case HM.lookup "status" o of
      Just "applied"    ->
        RevealApplied <$> (fmap unStringEncode <$> o .:? "consumed_gas")
                      <*> (fmap unStringEncode <$> o .:? "consumed_milligas")
      Just "failed"     ->
        RevealFailed <$> o .: "errors"
      Just "skipped"    -> pure RevealSkipped
      Just "backtracked" ->
        RevealBacktracked <$> (fmap unStringEncode <$> o .:? "consumed_gas")
                          <*> (fmap unStringEncode <$>o .:? "consumed_milligas")
                          <*> o .:? "errors"
      _ -> fail "Reveal expected a key 'status'."

data Transaction 
  = TransactionApplied
      { storage              :: Maybe Expression
      , bigMapDiff           :: Maybe [BigMapDiff]
      , balanceUpdates       :: Maybe [BalanceUpdate]
      , originatedContracts  :: Maybe [Text] -- ContractId]
      , consumedGas          :: Maybe Integer -- Bignum
      , consumedMilligas     :: Maybe Integer -- Bignum      
      , storageSize          :: Maybe Integer -- Bignum
      , paidStorageSizeDiff  :: Maybe Integer -- Bignum
      , allocatedDestinationContract :: Maybe Bool
      }
  | TransactionFailed
      { errors  :: [Error]
      }
  | TransactionSkipped
  | TransactionBacktracked
    { mErrors                      :: Maybe [Error]
    , storage                      :: Maybe Expression
    , bigMapDiff                   :: Maybe [BigMapDiff]
    , balanceUpdates               :: Maybe [BalanceUpdate]
    , originatedContracts          :: Maybe [Text] -- ContractId]
    , consumedGas                  :: Maybe Integer -- Bignum
    , consumedMilligas             :: Maybe Integer -- Bignum    
    , storageSize                  :: Maybe Integer -- Bignum
    , paidStorageSize              :: Maybe Integer -- Bignum
    , allocatedDestinationContract :: Maybe Bool
    }
  deriving (Eq, Show, Generic)

instance ToJSON Transaction where
  toJSON (TransactionApplied storage' bigMapDiff' balanceUpdates'
          originatedContracts' consumedGas' consumedMilligas' storageSize' paidStorageSizeDiff'
          allocatedDestination') =
    object $
      [ "status"                   .=  ("applied" :: Text )
      ] ++ catMaybes
      [ "storage"                .=? storage'
      , "big_map_diff"           .=? bigMapDiff'
      , "balance_updates"        .=? balanceUpdates'
      , "originated_contracts"   .=? originatedContracts'
      , "consumed_gas"           .=? (fmap StringEncode consumedGas')
      , "consumed_milligas"      .=? (fmap StringEncode consumedMilligas')
      , "storage_size"           .=? (fmap StringEncode storageSize')
      , "paid_storage_size_diff" .=? (fmap StringEncode paidStorageSizeDiff')
      , "allocated_destination_contract"  .=? allocatedDestination'
      ]
  toJSON (TransactionFailed errors') =
    object
      [ "status"   .= ("failed" :: Text )
      , "errors" .= errors'
      ]
  toJSON TransactionSkipped =
    object
      [ "status" .= ("skipped" :: Text )
      ]
  toJSON (TransactionBacktracked mErrors' storage' bigMapDiff' balanceUpdates'
          originatedContracts' consumedGas' consumedMilligas' storageSize' paidStorageSizeDiff'
          allocatedDestination') =
    object $
      [ "status"                 .=  ("backtracked" :: Text )
      ] ++ catMaybes
      [ "errors"                 .=? mErrors'
      , "storage"                .=? storage'
      , "big_map_diff"           .=? bigMapDiff'
      , "balance_updates"        .=? balanceUpdates'
      , "originated_contracts"   .=? originatedContracts'
      , "consumed_gas"           .=? (fmap StringEncode consumedGas')
      , "consumed_milligas"      .=? (fmap StringEncode consumedMilligas')
      , "storage_size"           .=? (fmap StringEncode storageSize')
      , "paid_storage_size_diff" .=? (fmap StringEncode paidStorageSizeDiff')
      , "allocated_destination_contract"  .=? allocatedDestination'
      ]
  
instance FromJSON Transaction where
  parseJSON = withObject "Transaction" $ \o ->
    case HM.lookup "status" o of
      Just "applied"    ->
        TransactionApplied <$> o .:? "storage"
                           <*> o .:? "big_map_diff"
                           <*> o .:? "balance_updates"
                           <*> o .:? "originated_contracts"
                           <*> (fmap unStringEncode <$> o .:? "consumed_gas")
                           <*> (fmap unStringEncode <$> o .:? "consumed_milligas")
                           <*> (fmap unStringEncode <$> o .:? "storage_size")
                           <*> (fmap unStringEncode <$> o .:? "paid_storage_size_diff")
                           <*> o .:? "allocated_destination_contract"
      Just "failed"     ->
        TransactionFailed <$> o .: "errors"
      Just "skipped"    -> pure TransactionSkipped
      Just "backtracked" ->
        TransactionBacktracked <$> o .:? "errors"
                               <*> o .:? "storage"
                               <*> o .:? "big_map_diff"
                               <*> o .:? "balance_updates"
                               <*> o .:? "originated_contracts"
                               <*> (fmap unStringEncode <$> o .:? "consumed_gas")
                               <*> (fmap unStringEncode <$> o .:? "consumed_milligas")
                               <*> (fmap unStringEncode <$> o .:? "storage_size")
                               <*> (fmap unStringEncode <$> o .:? "paid_storage_size_diff")
                               <*> o .:? "allocated_destination_contract"
      _ -> fail "Transaction expected a key 'status'."
