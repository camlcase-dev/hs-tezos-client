{-# LANGUAGE DataKinds           #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeApplications          #-}
{-# LANGUAGE TypeOperators       #-}

module Tezos.Client.Delphi.RPC where

import Data.Proxy (Proxy(..))
import Data.Text (Text)
import GHC.Natural (Natural)
import Servant.API
import Servant.Client.Core (RunClient, clientIn)
import Tezos.Client.Delphi.Block (Block)
import Tezos.Client.Delphi.Micheline.Expression (Expression)


type NodeRPC =
  -- GET block
   "chains" :> "main" :> "blocks" :> Capture "block-id" Text :> Get '[JSON] Block :<|>
  
  -- GET contract storage
   "chains" :> "main" :> "blocks"  :> Capture "block-id" Text :> "context" :> "contracts" :> Capture "contract-id" Text :> "storage" :> Get '[JSON] Expression :<|>

  -- GET big map
   "chains" :> "main" :> "blocks"  :> Capture "block-id" Text :> "context" :> "big_maps" :> Capture "big-map-id" Natural :> Capture "script-expr" Text :> Get '[JSON] Expression

nodeRPC :: Proxy NodeRPC
nodeRPC = Proxy

data RPCRoutes m =
  RPCRoutes
    { getBlock :: Text -> m Block
    , getContractStorage :: Text -> Text -> m Expression
    , getBigMap :: Text -> Natural -> Text -> m Expression
    }

rpcRoutes :: forall m. RunClient m => RPCRoutes m
rpcRoutes =
  RPCRoutes
    { getBlock = getBlock'
    , getContractStorage = getContractStorage'
    , getBigMap = getBigMap'
    }
  where
    (getBlock' :<|> getContractStorage' :<|> getBigMap') = nodeRPC `clientIn` (Proxy @m)
