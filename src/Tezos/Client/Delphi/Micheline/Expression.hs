{-# LANGUAGE DeriveGeneric              #-}
{-# LANGUAGE DerivingStrategies         #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE ScopedTypeVariables        #-}
{-# LANGUAGE TemplateHaskell            #-}

module Tezos.Client.Delphi.Micheline.Expression where

import Data.Aeson
import qualified Data.HashMap.Strict as HM
import Data.ByteString (ByteString)
import Data.Maybe (catMaybes)
import Data.Text (Text)
import qualified Data.Text as T
import Data.Vector (toList)
import GHC.Generics (Generic)
import Tezos.Client.Delphi.Micheline.Primitive (Primitive)
import Tezos.Client.Util.Aeson (HexJSONByteString(..), StringEncode(..))

data MichelinePrim = MichelinePrim
  { prim :: Primitive
  , args :: Maybe [Expression]
  , annots :: Maybe [T.Text]
  } deriving (Eq, Show, Generic)

instance ToJSON MichelinePrim where
  toJSON (MichelinePrim prim' args' annots') = object $ catMaybes
    [ Just ("prim" .= prim')
    , if args' == mempty then Nothing else Just ("args" .= args')
    , if annots' == mempty then Nothing else Just ("annots" .= annots')
    ]

instance FromJSON MichelinePrim where
  parseJSON =
    withObject "MichelinePrim" $ \o ->
    MichelinePrim
      <$> o .: "prim"
      <*> o .:? "args" .!= mempty
      <*> o .:? "annots" .!= mempty

data Expression
  = ExpressionInt Integer
  | ExpressionString T.Text
  | ExpressionBytes ByteString
  | Expressions [Expression]
  | ExpressionPrim MichelinePrim
  deriving (Eq, Show, Generic)

instance ToJSON Expression where
  toJSON (Expressions xs)     = toJSON xs
  toJSON (ExpressionPrim xs)  = toJSON xs
  toJSON (ExpressionString x) = object ["string" .= x]
  toJSON (ExpressionInt x)    = object ["int" .= StringEncode x]
  toJSON (ExpressionBytes x)  = object ["bytes" .= HexJSONByteString x]

instance FromJSON Expression where
  parseJSON v@(Object o) = do
    let hasInt    = HM.member "int" o
        hasString = HM.member "string" o
        hasBytes  = HM.member "bytes" o
        hasPrim   = HM.member "prim" o
        parseExpression
          | hasInt    = ExpressionInt . unStringEncode <$> o .: "int"
          | hasString = ExpressionString <$> o .: "string"
          | hasBytes  = ExpressionBytes . unHexJSONByteString <$> o .: "bytes"
          | hasPrim   = ExpressionPrim <$> parseJSON v
          | otherwise = fail $ "Expression found an object but it was not valid " ++ (show o)
    parseExpression

  parseJSON (Array xs) = do
    expressions <- sequence . toList $ (parseJSON <$> xs)
    pure $ Expressions expressions

  parseJSON json_ = fail $ "Expression expected an object or an array, found " ++ show json_

data Parameters =
  Parameters
    { _pEntrypoint :: Entrypoint
    , _pValue      :: Expression
    } deriving (Eq, Show, Generic)

instance ToJSON Parameters where
  toJSON (Parameters entrypoint' value') =
    object $
      [ "entrypoint" .= entrypoint'
      , "value"      .= value'
      ]

instance FromJSON Parameters where
  parseJSON = withObject "Parameters" $ \o ->
    Parameters <$> o .:  "entrypoint"
               <*> o .:  "value"

--------------------------------------------------------------------------------
-- Entrypoint
--------------------------------------------------------------------------------

-- $entrypoint:
--   /* entrypoint
--      Named entrypoint to a Michelson smart contract */
--   "default"
--   || "root"
--   || "do"
--   || "set_delegate"
--   || "remove_delegate"
--   || $unistring

data Entrypoint
  = EDefault
  | ERoot
  | EDo
  | ESetDelegate
  | ERemoveDelegate
  | EString Text
  deriving (Eq, Show, Generic, Ord)

instance ToJSON Entrypoint where
  toJSON EDefault        = "default"
  toJSON ERoot           = "root"
  toJSON EDo             = "do"
  toJSON ESetDelegate    = "set_delegate"
  toJSON ERemoveDelegate = "remove_delegate"
  toJSON (EString t)  = toJSON t

instance FromJSON Entrypoint where
  parseJSON (String s) =
    case s of
      "default"         -> pure EDefault
      "root"            -> pure ERoot
      "do"              -> pure EDo
      "set_delegate"    -> pure ESetDelegate
      "remove_delegate" -> pure ERemoveDelegate
      _                 -> pure $ EString s
  parseJSON json_ = fail $ "Entrypoint expected a string or an object, found " ++ (show json_)

data Contract =
  Contract
    { code    :: Expression
    , storage :: Expression
    } deriving (Eq, Show, Generic)

instance ToJSON Contract where
  toJSON (Contract code' storage') =
    object
      [ "code"    .= code'
      , "storage" .= storage'
      ]

instance FromJSON Contract where
  parseJSON = withObject "Contract" $ \o ->
    Contract <$> o .: "code"
              <*> o .: "storage"

data BigMap =
  BigMap
    { key   :: Expression
    , type_ :: Expression
    } deriving (Eq, Show, Generic)

instance ToJSON BigMap where
  toJSON (BigMap key' type') =
    object
      [ "key"  .= key'
      , "type" .= type'
      ]

instance FromJSON BigMap where
  parseJSON = withObject "BigMap" $ \o ->
    BigMap <$> o .: "key"
           <*> o .: "type"
