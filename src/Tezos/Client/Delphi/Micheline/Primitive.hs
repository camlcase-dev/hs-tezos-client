{-# LANGUAGE DeriveGeneric     #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell   #-}

module Tezos.Client.Delphi.Micheline.Primitive where

import qualified Data.Aeson as Aeson
import Data.Char (isLower)
import qualified Data.Text as T
import GHC.Generics (Generic)

data Primitive
  = PrimitiveInstruction PrimitiveInstruction
  | PrimitiveData PrimitiveData
  | PrimitiveType PrimitiveType
  deriving (Eq, Show, Generic)

instance Aeson.ToJSON Primitive where
  toJSON (PrimitiveInstruction primitiveInstruction') = Aeson.toJSON primitiveInstruction'
  toJSON (PrimitiveData primitiveData') = Aeson.toJSON primitiveData'
  toJSON (PrimitiveType primitiveType') = Aeson.toJSON primitiveType'

instance Aeson.FromJSON Primitive where
  parseJSON = Aeson.withText "Primitive" $ \t -> do
    let s = T.unpack t
        l = length s

    if l > 1
    then
      if isLower (head s)
      then
        PrimitiveType <$> Aeson.parseJSON (Aeson.String t)
      else
        if isLower (head . tail $ s)
        then
          PrimitiveData <$> Aeson.parseJSON (Aeson.String t)
        else
          PrimitiveInstruction <$> Aeson.parseJSON (Aeson.String t)
      
    else fail "Primitive"

data PrimitiveInstruction
  = PIAdd
  | PILe
  | PIUnit
  | PICompare
  | PILambda
  | PILoop
  | PIImplicitAccount
  | PINone
  | PIBlake2B
  | PISha256
  | PIXor
  | PIRename
  | PIMap
  | PISetDelegate
  | PIDip
  | PIPack
  | PISize
  | PIIfCons
  | PILsr
  | PITransferTokens
  | PIUpdate
  | PICdr
  | PISwap
  | PISome
  | PISha512
  | PICheckSignature
  | PIBalance
  | PIEmptyBigMap
  | PIEmptySet
  | PISub
  | PIMem
  | PIRight
  | PIAddress
  | PIConcat
  | PIUnpack
  | PINot
  | PILeft
  | PIAmount
  | PIDrop
  | PIAbs
  | PIGe
  | PIPush
  | PILt
  | PINeq
  | PINeg
  | PICons
  | PIExec
  | PINil
  | PIIsnat
  | PIMul
  | PILoopLeft
  | PIEdiv
  | PISlice
  | PIStepsToQuota
  | PIInt
  | PISource
  | PICar
  | PICreateAccount
  | PILsl
  | PIOr
  | PIIfNone
  | PISelf
  | PIIf
  | PISender
  | PIDup
  | PIEq
  | PINow
  | PIGet
  | PIGt
  | PIIfLeft
  | PIFailwith
  | PIPair
  | PIIter
  | PICast
  | PIEmptyMap
  | PICreateContract
  | PIHashKey
  | PIContract
  | PIAnd
  | PIDig
  | PIDug
  | PIApply
  deriving (Eq, Show, Generic)

instance Aeson.ToJSON PrimitiveInstruction where
  toJSON PIAdd             = "ADD"
  toJSON PILe              = "LE"
  toJSON PIUnit            = "UNIT"
  toJSON PICompare         = "COMPARE"
  toJSON PILambda          = "LAMBDA"
  toJSON PILoop            = "LOOP"
  toJSON PIImplicitAccount = "IMPLICIT_ACCOUNT"
  toJSON PINone            = "NONE"
  toJSON PIBlake2B         = "BLAKE2B"
  toJSON PISha256          = "SHA256"
  toJSON PIXor             = "XOR"
  toJSON PIRename          = "RENAME"
  toJSON PIMap             = "MAP"
  toJSON PISetDelegate     = "SET_DELEGATE"
  toJSON PIDip             = "DIP"
  toJSON PIPack            = "PACK"
  toJSON PISize            = "SIZE"
  toJSON PIIfCons          = "IF_CONS"
  toJSON PILsr             = "LSR"
  toJSON PITransferTokens  = "TRANSFER_TOKENS"
  toJSON PIUpdate          = "UPDATE"
  toJSON PICdr             = "CDR"
  toJSON PISwap            = "SWAP"
  toJSON PISome            = "SOME"
  toJSON PISha512          = "SHA512"
  toJSON PICheckSignature  = "CHECK_SIGNATURE"
  toJSON PIBalance         = "BALANCE"
  toJSON PIEmptyBigMap     = "EMPTY_BIG_MAP"
  toJSON PIEmptySet        = "EMPTY_SET"
  toJSON PISub             = "SUB"
  toJSON PIMem             = "MEM"
  toJSON PIRight           = "RIGHT"
  toJSON PIAddress         = "ADDRESS"
  toJSON PIConcat          = "CONCAT"
  toJSON PIUnpack          = "UNPACK"
  toJSON PINot             = "NOT"
  toJSON PILeft            = "LEFT"
  toJSON PIAmount          = "AMOUNT"
  toJSON PIDrop            = "DROP"
  toJSON PIAbs             = "ABS"
  toJSON PIGe              = "GE"
  toJSON PIPush            = "PUSH"
  toJSON PILt              = "LT"
  toJSON PINeq             = "NEQ"
  toJSON PINeg             = "NEG"
  toJSON PICons            = "CONS"
  toJSON PIExec            = "EXEC"
  toJSON PINil             = "NIL"
  toJSON PIIsnat           = "ISNAT"
  toJSON PIMul             = "MUL"
  toJSON PILoopLeft        = "LOOP_LEFT"
  toJSON PIEdiv            = "EDIV"
  toJSON PISlice           = "SLICE"
  toJSON PIStepsToQuota    = "STEPS_TO_QUOTA"
  toJSON PIInt             = "INT"
  toJSON PISource          = "SOURCE"
  toJSON PICar             = "CAR"
  toJSON PICreateAccount   = "CREATE_ACCOUNT"
  toJSON PILsl             = "LSL"
  toJSON PIOr              = "OR"
  toJSON PIIfNone          = "IF_NONE"
  toJSON PISelf            = "SELF"
  toJSON PIIf              = "IF"
  toJSON PISender          = "SENDER"
  toJSON PIDup             = "DUP"
  toJSON PIEq              = "EQ"
  toJSON PINow             = "NOW"
  toJSON PIGet             = "GET"
  toJSON PIGt              = "GT"
  toJSON PIIfLeft          = "IF_LEFT"
  toJSON PIFailwith        = "FAILWITH"
  toJSON PIPair            = "PAIR"
  toJSON PIIter            = "ITER"
  toJSON PICast            = "CAST"
  toJSON PIEmptyMap        = "EMPTY_MAP"
  toJSON PICreateContract  = "CREATE_CONTRACT"
  toJSON PIHashKey         = "HASH_KEY"
  toJSON PIContract        = "CONTRACT"
  toJSON PIAnd             = "AND"
  toJSON PIDig             = "DIG"
  toJSON PIDug             = "DUG"
  toJSON PIApply           = "APPLY"

    
instance Aeson.FromJSON PrimitiveInstruction where
  parseJSON = Aeson.withText "PrimitiveInstruction" $ \s ->
    case s of
      "ADD"              -> pure PIAdd
      "LE"               -> pure PILe
      "UNIT"             -> pure PIUnit
      "COMPARE"          -> pure PICompare
      "LAMBDA"           -> pure PILambda
      "LOOP"             -> pure PILoop
      "IMPLICIT_ACCOUNT" -> pure PIImplicitAccount
      "NONE"             -> pure PINone
      "BLAKE2B"          -> pure PIBlake2B
      "SHA256"           -> pure PISha256
      "XOR"              -> pure PIXor
      "RENAME"           -> pure PIRename
      "MAP"              -> pure PIMap
      "SET_DELEGATE"     -> pure PISetDelegate
      "DIP"              -> pure PIDip
      "PACK"             -> pure PIPack
      "SIZE"             -> pure PISize
      "IF_CONS"          -> pure PIIfCons
      "LSR"              -> pure PILsr
      "TRANSFER_TOKENS"  -> pure PITransferTokens
      "UPDATE"           -> pure PIUpdate
      "CDR"              -> pure PICdr
      "SWAP"             -> pure PISwap
      "SOME"             -> pure PISome
      "SHA512"           -> pure PISha512
      "CHECK_SIGNATURE"  -> pure PICheckSignature
      "BALANCE"          -> pure PIBalance
      "EMPTY_BIG_MAP"    -> pure PIEmptyBigMap
      "EMPTY_SET"        -> pure PIEmptySet
      "SUB"              -> pure PISub
      "MEM"              -> pure PIMem
      "RIGHT"            -> pure PIRight
      "ADDRESS"          -> pure PIAddress
      "CONCAT"           -> pure PIConcat
      "UNPACK"           -> pure PIUnpack
      "NOT"              -> pure PINot
      "LEFT"             -> pure PILeft
      "AMOUNT"           -> pure PIAmount
      "DROP"             -> pure PIDrop
      "ABS"              -> pure PIAbs
      "GE"               -> pure PIGe
      "PUSH"             -> pure PIPush
      "LT"               -> pure PILt
      "NEQ"              -> pure PINeq
      "NEG"              -> pure PINeg
      "CONS"             -> pure PICons
      "EXEC"             -> pure PIExec
      "NIL"              -> pure PINil
      "ISNAT"            -> pure PIIsnat
      "MUL"              -> pure PIMul
      "LOOP_LEFT"        -> pure PILoopLeft
      "EDIV"             -> pure PIEdiv
      "SLICE"            -> pure PISlice
      "STEPS_TO_QUOTA"   -> pure PIStepsToQuota
      "INT"              -> pure PIInt
      "SOURCE"           -> pure PISource
      "CAR"              -> pure PICar
      "CREATE_ACCOUNT"   -> pure PICreateAccount
      "LSL"              -> pure PILsl
      "OR"               -> pure PIOr
      "IF_NONE"          -> pure PIIfNone
      "SELF"             -> pure PISelf
      "IF"               -> pure PIIf
      "SENDER"           -> pure PISender
      "DUP"              -> pure PIDup
      "EQ"               -> pure PIEq
      "NOW"              -> pure PINow
      "GET"              -> pure PIGet
      "GT"               -> pure PIGt
      "IF_LEFT"          -> pure PIIfLeft
      "FAILWITH"         -> pure PIFailwith
      "PAIR"             -> pure PIPair
      "ITER"             -> pure PIIter
      "CAST"             -> pure PICast
      "EMPTY_MAP"        -> pure PIEmptyMap
      "CREATE_CONTRACT"  -> pure PICreateContract
      "HASH_KEY"         -> pure PIHashKey
      "CONTRACT"         -> pure PIContract
      "AND"              -> pure PIAnd
      "DIG"              -> pure PIDig
      "DUG"              -> pure PIDug
      "APPLY"            -> pure PIApply
      _                  -> fail $ "PrimitiveInstruction found unknown json " ++ show s

      

data PrimitiveData
  = PDElt
  | PDRight
  | PDFalse
  | PDUnit
  | PDSome
  | PDNone
  | PDLeft
  | PDTrue
  | PDPair
  deriving (Eq, Show, Generic)


instance Aeson.ToJSON PrimitiveData where
  toJSON PDElt   = "Elt"
  toJSON PDRight = "Right"
  toJSON PDFalse = "False"
  toJSON PDUnit  = "Unit"
  toJSON PDSome  = "Some"
  toJSON PDNone  = "None"
  toJSON PDLeft  = "Left"
  toJSON PDTrue  = "True"
  toJSON PDPair  = "Pair"

instance Aeson.FromJSON PrimitiveData where
  parseJSON = Aeson.withText "PrimitiveData" $ \s ->
    case s of
      "Elt"   -> pure PDElt
      "Right" -> pure PDRight
      "False" -> pure PDFalse
      "Unit"  -> pure PDUnit
      "Some"  -> pure PDSome
      "None"  -> pure PDNone
      "Left"  -> pure PDLeft
      "True"  -> pure PDTrue
      "Pair"  -> pure PDPair
      _ -> fail "PrimitiveData"

data PrimitiveType
  = PTTimestamp
  | PTSignature
  | PTSet
  | PTPair
  | PTBytes
  | PTAddress
  | PTOr
  | PTList
  | PTStorage
  | PTKeyHash
  | PTUnit
  | PTOption
  | PTBigMap
  | PTString
  | PTMutez
  | PTBool
  | PTOperation
  | PTContract
  | PTMap
  | PTNat
  | PTKey
  | PTLambda
  | PTInt
  | PTParameter
  | PTCode
  deriving (Eq, Show, Generic)

instance Aeson.ToJSON PrimitiveType where
  toJSON PTTimestamp = "timestamp"
  toJSON PTSignature = "signature"
  toJSON PTSet       = "set"
  toJSON PTPair      = "pair"
  toJSON PTBytes     = "bytes"
  toJSON PTAddress   = "address"
  toJSON PTOr        = "or"
  toJSON PTList      = "list"
  toJSON PTStorage   = "storage"
  toJSON PTKeyHash   = "key_hash"
  toJSON PTUnit      = "unit"
  toJSON PTOption    = "option"
  toJSON PTBigMap    = "big_map"
  toJSON PTString    = "string"
  toJSON PTMutez     = "mutez"
  toJSON PTBool      = "bool"
  toJSON PTOperation = "operation"
  toJSON PTContract  = "contract"
  toJSON PTMap       = "map"
  toJSON PTNat       = "nat"
  toJSON PTKey       = "key"
  toJSON PTLambda    = "lambda"
  toJSON PTInt       = "int"
  toJSON PTParameter = "parameter"
  toJSON PTCode      = "code"

instance Aeson.FromJSON PrimitiveType where
  parseJSON = Aeson.withText "PrimitiveType" $ \s ->
    case s of
      "timestamp"  -> pure PTTimestamp
      "signature"  -> pure PTSignature
      "set"        -> pure PTSet
      "pair"       -> pure PTPair
      "bytes"      -> pure PTBytes
      "address"    -> pure PTAddress
      "or"         -> pure PTOr
      "list"       -> pure PTList
      "storage"    -> pure PTStorage
      "key_hash"   -> pure PTKeyHash
      "unit"       -> pure PTUnit
      "option"     -> pure PTOption
      "big_map"    -> pure PTBigMap
      "string"     -> pure PTString
      "mutez"      -> pure PTMutez
      "bool"       -> pure PTBool
      "operation"  -> pure PTOperation
      "contract"   -> pure PTContract
      "map"        -> pure PTMap
      "nat"        -> pure PTNat
      "key"        -> pure PTKey
      "lambda"     -> pure PTLambda
      "int"        -> pure PTInt
      "parameter"  -> pure PTParameter
      "code"       -> pure PTCode
      _ -> fail "PrimitiveType"
