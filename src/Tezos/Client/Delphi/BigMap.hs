{-# LANGUAGE DeriveGeneric     #-}
{-# LANGUAGE DuplicateRecordFields     #-}
{-# LANGUAGE LambdaCase        #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell   #-}

module Tezos.Client.Delphi.BigMap where

import qualified Data.Aeson as Aeson
import Data.Aeson (ToJSON(..), FromJSON(..), object, withObject, (.:), (.:?), (.=))
import qualified Data.HashMap.Lazy as HM
import Data.Maybe (catMaybes)
import Data.Text (Text)
import GHC.Generics (Generic)
import Tezos.Client.Delphi.Micheline.Expression (Expression)
import Tezos.Client.Util.Aeson ((.=?), StringEncode(..))


data BigMapDiffUpdate
  = BMDU
    { bigMap  :: Integer
    , keyHash :: Text -- ScriptExpr
    , key     :: Expression
    , value   :: Maybe Expression
    } deriving (Eq, Show, Generic)

instance ToJSON BigMapDiffUpdate where
  toJSON (BMDU bigMap' keyHash' key' value') = 
    object $ [ "action" .= ("update" :: Text)
             , "big_map" .= StringEncode bigMap'
             , "key_hash" .= keyHash'
             , "key" .= key'
             ] ++ catMaybes ["value" .=? value']


instance FromJSON BigMapDiffUpdate where
  parseJSON = withObject "BigMapDiffUpdate" $ \o ->
    BMDU <$> (fmap unStringEncode $ o .: "big_map")
         <*> o .:  "key_hash"
         <*> o .:  "key"
         <*> o .:? "value"
    
data BigMapDiffRemove
  = BMDR
    { bigMap :: Integer
    } deriving (Eq, Show, Generic)

instance ToJSON BigMapDiffRemove where
  toJSON (BMDR bigMap') = 
    object [ "action" .= ("remove" :: Text)
           , "big_map" .= StringEncode bigMap'
           ]

instance FromJSON BigMapDiffRemove where
  parseJSON = withObject "BigMapDiffRemove" $ \o ->
    BMDR . unStringEncode <$> o .: "big_map"

data BigMapDiffCopy
  = BMDC
    { sourceBigMap :: Integer
    , destinationBigMap :: Integer
    } deriving (Eq, Show, Generic)

instance ToJSON BigMapDiffCopy where
  toJSON (BMDC sourceBigMap' destinationBigMap') = 
    object [ "action" .= ("copy" :: Text)
           , "source_big_map" .= StringEncode sourceBigMap'
           , "destination_big_map" .= StringEncode destinationBigMap'
           ]

instance FromJSON BigMapDiffCopy where
  parseJSON = withObject "BigMapDiffCopy" $ \o ->
    BMDC <$> (fmap unStringEncode $ o .: "source_big_map")
         <*> (fmap unStringEncode $ o .: "destination_big_map")

data BigMapDiffAlloc
  = BMDA
    { bigMap    :: Integer
    , keyType   :: Expression
    , valueType :: Expression
    } deriving (Eq, Show, Generic)

instance ToJSON BigMapDiffAlloc where
  toJSON (BMDA bigMap' keyType' valueType') =
    object [ "action"     .= ("alloc" :: Text)
           , "big_map"    .= StringEncode bigMap'
           , "key_type"   .= keyType'
           , "value_type" .= valueType'
           ]

instance FromJSON BigMapDiffAlloc where
  parseJSON = withObject "BigMapDiffAlloc" $ \o ->
    BMDA <$> (fmap unStringEncode $ o .: "big_map")
         <*> o .: "key_type"
         <*> o .: "value_type"

data BigMapDiff
  = BigMapDiffUpdate BigMapDiffUpdate
  | BigMapDiffRemove BigMapDiffRemove
  | BigMapDiffCopy BigMapDiffCopy
  | BigMapDiffAlloc BigMapDiffAlloc
  deriving (Eq, Show, Generic)

instance ToJSON BigMapDiff where
  toJSON = \case
    BigMapDiffUpdate o -> toJSON o
    BigMapDiffRemove o -> toJSON o
    BigMapDiffCopy o -> toJSON o
    BigMapDiffAlloc o -> toJSON o

instance FromJSON BigMapDiff where
  parseJSON v@(Aeson.Object o) =
    case HM.lookup "action" o of
      Just "update" ->
        BigMapDiffUpdate <$> parseJSON v
      Just "remove" ->
        BigMapDiffRemove <$> parseJSON v

      Just "copy"   ->
        BigMapDiffCopy   <$> parseJSON v

      Just "alloc"  ->
        BigMapDiffAlloc  <$> parseJSON v

      _ -> fail "BigMapDiff expected a key 'kind'."
  parseJSON _ = fail "BigMapDiff expected an object."
