{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE OverloadedStrings     #-}

module Tezos.Client.Delphi.Vote where

import Data.Aeson
  (FromJSON(parseJSON), ToJSON(toJSON), (.=), (.:), object, withObject, withText)
import Data.Int (Int64)
import Data.Text (Text)
import GHC.Generics (Generic)

data Ballot
  = Yay
  | Nay
  | Pass
  deriving (Eq, Show, Generic, Ord)

instance ToJSON Ballot where
  toJSON Yay  = "yay"
  toJSON Nay  = "nay"
  toJSON Pass = "pass"

instance FromJSON Ballot where
  parseJSON = withText "Ballot" $ \t ->
    case t of
      "yay" -> pure Yay
      "nay" -> pure Nay
      "pass" -> pure Pass
      err -> error (show err)

data Vote =
  Vote
    { pkh     :: Text
    , vballot :: Ballot
    }  deriving (Eq, Show, Generic)

instance ToJSON Vote where
  toJSON (Vote pkh' ballot') =
    object
      [ "pkh"    .= pkh'
      , "ballot" .= ballot'
      ]

instance FromJSON Vote where
  parseJSON = withObject "Vote" $ \o ->
    Vote <$> o .: "pkh"
         <*> o .: "ballot"

data BallotCount =
  BallotCount
    { yay  :: Int64
    , nay  :: Int64
    , pass :: Int64
    } deriving (Eq, Show, Generic)

instance ToJSON BallotCount where
  toJSON (BallotCount yay' nay' pass') =
    object
      [ "yay"  .= yay'
      , "nay"  .= nay'
      , "pass" .= pass'
      ] 

instance FromJSON BallotCount where
  parseJSON = withObject "BallotCount" $ \o ->
    BallotCount <$> o .: "yay"
                <*> o .: "nay"
                <*> o .: "pass"

data VotingPeriodKind
  = Proposal
  | TestingVote
  | Testing
  | PromotionVote
  deriving (Eq, Show, Generic)

instance ToJSON VotingPeriodKind where
  toJSON Proposal      = "proposal"
  toJSON TestingVote   = "testing_vote"
  toJSON Testing       = "testing"
  toJSON PromotionVote = "promotion_vote"

instance FromJSON VotingPeriodKind where
  parseJSON = withText "VotingPeriodKind" $ \t ->
    case t of
      "proposal"       -> pure Proposal
      "testing_vote"   -> pure TestingVote
      "testing"        -> pure Testing
      "promotion_vote" -> pure PromotionVote
      _ -> fail "VotingPeriodKind"

data DelegateWeight =
  DelegateWeight
    { pkh :: Text
    , rolls :: Int64
    } deriving (Eq, Show, Generic)

instance ToJSON DelegateWeight where
  toJSON (DelegateWeight pkh' rolls') =
    object
      [ "pkh"   .= pkh'
      , "rolls" .= rolls'
      ]

instance FromJSON DelegateWeight where
  parseJSON = withObject "DelegateWeight" $ \o ->
    DelegateWeight <$> o .: "pkh"
                   <*> o .: "rolls"
