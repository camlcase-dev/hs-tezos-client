{-# LANGUAGE DeriveGeneric              #-}
{-# LANGUAGE DerivingStrategies         #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE ScopedTypeVariables        #-}

module Tezos.Client.Util.Aeson where

import Data.Aeson (ToJSON(..), FromJSON(..), (.=), withScientific, withText)
import Data.Aeson.Types (Pair, Parser, Value(Object,Array,String),typeMismatch)
import Data.ByteString (ByteString)
import Data.Proxy (Proxy(Proxy))
import Data.Hashable 
import qualified Data.Scientific
import qualified Data.Text as T
import Data.Text  (Text)
import Data.Typeable (Typeable, typeRep)
import GHC.Generics (Generic)
import Text.Hex (decodeHex, encodeHex)
import Text.Read (readEither)

--- | Newtype wrapper for Integer which uses String representation
--- for JSON serialization.
newtype StringEncode a = StringEncode { unStringEncode :: a }
  deriving (Generic, Eq, Ord, Read, Show)

instance (Show a) => ToJSON (StringEncode a) where
  toJSON (StringEncode x) = String $ T.pack $ show x

instance (Read a, Typeable a) => FromJSON (StringEncode a) where
  parseJSON = withText (show $ typeRep (Proxy :: Proxy a)) $ \txt ->
    StringEncode <$>
    (either fail pure $ readEither (T.unpack txt))

--- | Newtype wrapper for ByteString which uses hexadecimal representation
--- for JSON serialization.
newtype HexJSONByteString = HexJSONByteString { unHexJSONByteString :: ByteString }
  deriving stock (Eq, Ord, Show, Generic)
  deriving newtype (Hashable)

instance ToJSON HexJSONByteString where
  toJSON = toJSON . encodeHex . unHexJSONByteString

instance FromJSON HexJSONByteString where
  parseJSON =
    withText "Hex-encoded bytestring" $ \t ->
      case decodeHex t of
        Nothing -> fail "Invalid hex encoding"
        Just res -> pure (HexJSONByteString res)

-- | avoid {"key":null} if Maybe is Nothing
mkPairOnlyIfValueIsJust :: ToJSON v => Text -> Maybe v -> Maybe Pair
mkPairOnlyIfValueIsJust = (.=?)

(.=?) :: ToJSON v => Text -> Maybe v -> Maybe Pair
(.=?) name mValue =
  case mValue of
    Nothing    -> Nothing
    Just value -> Just $ name .= value
infixr 8 .=?
{-# INLINE (.=?) #-}

withInteger
  :: (Bounded n, Integral n)
  => String
  -> (n -> Parser a)
  -> Value
  -> Parser a
withInteger expected f x = withScientific expected f' x
  where
    f' = maybe (typeMismatch expected x) f . Data.Scientific.toBoundedInteger

-- | combine aeson values only if they are the same constructor and are
-- semigroups.
combineValues :: Value -> Value -> Value
combineValues (Object o1) (Object o2) = Object $ o1 <> o2
combineValues (Array  o1) (Array  o2) = Array  $ o1 <> o2
combineValues (String o1) (String o2) = String $ o1 <> o2
combineValues v1 _v2 = v1
