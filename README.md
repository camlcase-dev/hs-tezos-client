# hs-tezos-client

`hs-tezos-client` is a small collection of types and servant routes for the 
Tezos node. The current scope is reading data from blocks. Some types are directly
borrowed or inspired by the [morley](https://gitlab.com/morley-framework/morley) 
library.
